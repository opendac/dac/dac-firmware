/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define GPIO_OUT_CH3_Pin GPIO_PIN_13
#define GPIO_OUT_CH3_GPIO_Port GPIOC
#define GPIO_OUT_CH2_Pin GPIO_PIN_14
#define GPIO_OUT_CH2_GPIO_Port GPIOC
#define GPIO_OUT_CH1_Pin GPIO_PIN_15
#define GPIO_OUT_CH1_GPIO_Port GPIOC
#define GPIO_ADC_CH1_Pin GPIO_PIN_0
#define GPIO_ADC_CH1_GPIO_Port GPIOA
#define GPIO_ADC_CH2_Pin GPIO_PIN_1
#define GPIO_ADC_CH2_GPIO_Port GPIOA
#define USART2_TX_Pin GPIO_PIN_2
#define USART2_TX_GPIO_Port GPIOA
#define GPIO_ADC_CH3_Pin GPIO_PIN_4
#define GPIO_ADC_CH3_GPIO_Port GPIOA
#define GPIO_SPI_SCK_Pin GPIO_PIN_5
#define GPIO_SPI_SCK_GPIO_Port GPIOA
#define GPIO_SPI_MISO_Pin GPIO_PIN_6
#define GPIO_SPI_MISO_GPIO_Port GPIOA
#define GPIO_SPI_MOSI_Pin GPIO_PIN_7
#define GPIO_SPI_MOSI_GPIO_Port GPIOA
#define GPIO_ADC_CH4_Pin GPIO_PIN_0
#define GPIO_ADC_CH4_GPIO_Port GPIOB
#define GPIO_OUT_CE_Pin GPIO_PIN_1
#define GPIO_OUT_CE_GPIO_Port GPIOB
#define GPIO_IN_CH1_Pin GPIO_PIN_12
#define GPIO_IN_CH1_GPIO_Port GPIOB
#define GPIO_IN_CH3_Pin GPIO_PIN_13
#define GPIO_IN_CH3_GPIO_Port GPIOB
#define GPIO_IN_CH2_Pin GPIO_PIN_14
#define GPIO_IN_CH2_GPIO_Port GPIOB
#define GPIO_IN_IRQ_Pin GPIO_PIN_15
#define GPIO_IN_IRQ_GPIO_Port GPIOB
#define GPIO_IN_IRQ_EXTI_IRQn EXTI15_10_IRQn
#define GPIO_ENC1_CHA_Pin GPIO_PIN_8
#define GPIO_ENC1_CHA_GPIO_Port GPIOA
#define GPIO_ENC1_CHB_Pin GPIO_PIN_9
#define GPIO_ENC1_CHB_GPIO_Port GPIOA
#define GPIO_OUT_CS_Pin GPIO_PIN_10
#define GPIO_OUT_CS_GPIO_Port GPIOA
#define GPIO_ENC2_CHA_Pin GPIO_PIN_15
#define GPIO_ENC2_CHA_GPIO_Port GPIOA
#define GPIO_ENC2_CHB_Pin GPIO_PIN_3
#define GPIO_ENC2_CHB_GPIO_Port GPIOB
#define GPIO_ENC3_CHA_Pin GPIO_PIN_4
#define GPIO_ENC3_CHA_GPIO_Port GPIOB
#define GPIO_ENC3_CHB_Pin GPIO_PIN_5
#define GPIO_ENC3_CHB_GPIO_Port GPIOB
#define GPIO_PWM_CH1_Pin GPIO_PIN_6
#define GPIO_PWM_CH1_GPIO_Port GPIOB
#define GPIO_PWM_CH2_Pin GPIO_PIN_7
#define GPIO_PWM_CH2_GPIO_Port GPIOB
#define GPIO_PWM_CH3_Pin GPIO_PIN_8
#define GPIO_PWM_CH3_GPIO_Port GPIOB
#define GPIO_PWM_CH4_Pin GPIO_PIN_9
#define GPIO_PWM_CH4_GPIO_Port GPIOB

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
