# dac-firmware

This repository holds the firmware to be downloaded to the microcontroller 
STM32F103C8T6 (compatible with blue pill board).

## Getting started

In short, you only need to download the most recent firmware (a hex file) and flash it to the board.

The easiest way to flash the firmware is using an [ST-LINK](https://www.st.com/en/development-tools/st-link-v2.html)
with the [STM32CubeProgr](https://www.st.com/en/development-tools/stm32cubeprog.html). You may look at this 
[video](https://www.youtube.com/watch?v=KgR3uM21y7o) to find some guidance of how to do that.

It also may be possible to program it with an 
[usb to serial converter](https://circuitdigest.com/microcontroller-projects/programming-stm32f103c8-board-using-usb-port).

## I want to develop or edit the firmware to match my needs

For that you will need to be able to edit and compile the code. For that, I use the GNU Arm None Eabi compilers and I 
manage the project using CMake + STM32CubeMX. 
