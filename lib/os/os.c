#include "os.h"
#include "lib/utils/list.h"

static osTask_t* runningTask = NULL;
static uint8_t osMemoryToTasks[osMaxTasks * sizeof(osTask_t)];
static list_t allTasks;

void osBlock(osTask_t* task)
{
    task->state = osTaskStateBlocked;
}

void osSuspend(osTask_t* task)
{
    task->state = osTaskStateSuspended;
}

void osResume(osTask_t* task)
{
    task->state = osTaskStateReady;
}

void osInit()
{
    listInit(&allTasks, osMaxTasks, sizeof(osTask_t), osMemoryToTasks);
}

void osDelayUs(osTask_t* task, uint32_t usecs)
{
    task->timeNextExecutionUs = getTimeUs() + usecs;
    osBlock(task);
}

void osDelayMs(osTask_t* task, uint32_t msecs)
{
    osDelayUs(task, 1000 * msecs);
}

void osScheduleTaskUs(osTask_t* task, uint32_t usecs)
{
    task->timeNextExecutionUs = getTimeUs() + usecs;
}

osTask_t* osCreateTask(void (*function)(), uint8_t priority)
{
    // Create a new task
    osTask_t* new_task = (osTask_t*) listReserve(&allTasks);
    new_task->state = osTaskStateSuspended;
    new_task->timeNextExecutionUs = 0;
    new_task->function = function;
    new_task->priority = priority;
    return new_task;
}

static osTask_t* osGetTaskToRun()
{
    // auxiliary variables
    osTask_t* task = NULL;
    osTask_t* auxTask;
    uint16_t id = 0;

    // Try to find the first ready task
    while ((task == NULL) && (id < listCount(&allTasks)))
    {
        auxTask = listPeek(&allTasks, id);
        if (auxTask->state == osTaskStateReady)
        {
            task = auxTask;
        }
        id = id + 1;
    }

    // If a ready task was found, then search for other ready tasks with higher priority
    if (task != NULL)
    {
        for (; id < listCount(&allTasks); id++)
        {
            auxTask = listPeek(&allTasks, id);
            if ((auxTask->priority > task->priority))
            {
                task = auxTask;
            }
        }
    }

    return task;
}

static void osUnblockScheduledTasks()
{
    uint16_t id;
    osTask_t* task;

    for (id = 0; id < listCount(&allTasks); id++)
    {
        task = listPeek(&allTasks, id);
        if (task->state == osTaskStateBlocked && (getTimeUs() >= task->timeNextExecutionUs))
        {
            task->state = osTaskStateReady;
        }
    }
}

void osRunScheduler()
{
    osUnblockScheduledTasks();
    runningTask = osGetTaskToRun();
    while (runningTask != NULL)
    {
        runningTask->function();
        runningTask = osGetTaskToRun();
    }
}
