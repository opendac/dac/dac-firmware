#ifndef LIB_OS_MEMORY_H
#define LIB_OS_MEMORY_H

#include "lib/utils/types.h"

typedef struct memBlock {
    uint32_t size;
    void* nextFreeByte;
} memBlock_t;

void osMemInit();

void* osMalloc(uint32_t wantedSize);

void osFree();

#endif //LIB_OS_MEMORY_H
