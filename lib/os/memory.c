#include "lib/os/memory.h"
#include <string.h>

static uint8_t osMemory[1024];
static uint32_t osFreeMemory;

void osMemInit()
{
    // Make sure heap is initialized with zeros
    memset(osMemory, 0, sizeof(osMemory));
    osFreeMemory = sizeof(osMemory);
}

void* osMalloc(uint32_t wantedSize)
{
    const uint32_t alignedSize = (wantedSize % 4 == 0) ? wantedSize : wantedSize + 4 - (wantedSize % 4);
    memBlock_t* block;
    memBlock_t* previousBlock;
    memBlock_t* newBlock;
    void* ptr;

    if (alignedSize > osFreeMemory)
    {
        // No enough memory available
        ptr = NULL;
    }
    else
    {
        previousBlock = (memBlock_t*) &osMemory[0];
        while ()
        {
            block = previousBlock->nextFreeByte;
        }
    }

    return ptr;
}
