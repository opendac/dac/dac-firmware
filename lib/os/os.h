#ifndef OS_H_
#define OS_H_

/**
 ***********************************************************************************************************************
 * @file    os.h
 * @author  José Roberto Colombo Junior
 * @brief   Header file of the task manager.
 ***********************************************************************************************************************
 *
 * This file implements basic support to a task manager. No dynamic memory is used. No preemption.
 *
 * @attention This is free software licensed under GPL.
 *
 ***********************************************************************************************************************
 */

#include "lib/utils/time.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * The max number os tasks that the scheduler will manage.
 */
#define osMaxTasks 8

/**
 * The task states.
 */
typedef enum osTaskState {
    osTaskStateReady = 0,        ///< The task in this state is waiting the tasks with more priority to finish running.
    osTaskStateBlocked,          ///< The task is running but it is waiting something to happen. In this case other tasks may run. If the timeout occurs, then the task in this state will be READY.
    osTaskStateSuspended,        ///< The task is running but it is waiting indefinitely something to happen. In this case other tasks may run.
} osTaskState_t;

/**
 * The model of a task.
 */
typedef struct osTask
{
    void (*function)();            ///< A pointer to the function that will run.
    osTaskState_t state;           ///< The task state.
    uint8_t priority;              ///< Higher is more important.
    uint32_t timeNextExecutionUs;  ///< The time of next execution.
} osTask_t;

typedef struct osMutex {
    uint8_t taken;
} osMutex_t;

/**
 * This function initializes the scheduler and must be called before calling the scheduler or creating any task.
 */
extern void osInit();

/**
 * Schedule a task to wait for milli seconds.
 * @warning It is supposed that this function should be called immediately before the task function returns.
 * @param[in] task is a handler to the task. If NULL is given, then the current task running will be used.
 * @param[in] msecs is how many milli seconds the task should wait.
 */
extern void osDelayMs(osTask_t* task, uint32_t msecs);

/**
 * Make a task to wait for milli seconds.
 * @warning It is supposed that this function should be called immediately before the task function returns.
 * @param[in] task is a handler to the task. If NULL is given, then the current task running will be used.
 * @param[in] usecs is how many micro seconds the task should wait.
 */
extern void osDelayUs(osTask_t* task, uint32_t usecs);

/**
 * @brief Schedule a task to run after usecs.
 * This function has the purpose of helping to keep some task running at desired rate easily. It should be used with
 * osBlock. The idea is: at the beginning of a task function, call osScheduleTask to schedule the next execution more
 * precisely. After this, do the task work. The last command in task function should be osBlock.
 * @param task
 * @param usecs
 */
extern void osScheduleTaskUs(osTask_t* task, uint32_t usecs);

/**
 * Blocks any task.
 * @param[in] task is a handle to the task that should be suspended.
 */
extern void osBlock(osTask_t* task);

/**
 * Suspends any task.
 * @param[in] task is a handle to the task that should be suspended.
 */
extern void osSuspend(osTask_t *task);

/**
 * Resumes any task. It can be used to abort delay.
 * @param[in] task is a handle to the task that should be resumed.
 */
extern void osResume(osTask_t *task);

/**
 * Create a task.
 * @return a pointer to the task control block.
 */
extern osTask_t *osCreateTask(void (*function)(), uint8_t priority);

/**
 * Call this function to run the scheduler. It is a good choice to place this function inside a while(1) in main
 * function.
 */
extern void osRunScheduler();

#ifdef __cplusplus
}
#endif

#endif /* OS_H_ */
