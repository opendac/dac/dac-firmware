#ifndef DSP_MOVING_AVERAGE_H_
#define DSP_MOVING_AVERAGE_H_

#include "lib/utils/types.h"
#include "lib/utils/math.h"

namespace dsp {

template<typename type, uint16_t maxSize=8>
class MovingAverage {
public:
    MovingAverage()
    {
        index = 0;
        sum = 0;
        length = maxSize;
    }

    ~MovingAverage() = default;

    void setLength(uint16_t newLength)
    {
        length = mathMinUnsigned(newLength, maxSize);
    }

    void add(type value)
    {
        sum = sum - values[index];
        values[index] = value;
        sum = sum + values[index];
        index = (index == (length - 1)) ? 0 : index + 1;
    }

    type getAverage()
    {
        return sum / length;
    }

    type getSum()
    {
        return sum;
    }

    type* getValues()
    {
        return values;
    }

    void reset(type value = 0)
    {
        uint16_t i;
        for (i = 0; i < length; i++)
        {
            values[i] = value;
        }

        index = 0;
        sum = length * value;
    }

protected:
    uint16_t length;
    type values[maxSize];
    type sum;
    uint16_t index;
};

} // namespace dsp

#endif /* DSP_MOVING_AVERAGE_H_ */
