#ifndef UTILS_LIST_H
#define UTILS_LIST_H

#include "lib/utils/types.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct list {    
    uint8_t* storage;              ///< A pointer to memory that will be used exclusively by the list. It must have at least maxNumberOfElements * itemSize bytes.
    uint8_t itemSize;              ///< The size of each element that will be stored in the list.
    uint16_t maxNumberOfElements;  ///< The maximum number of elements that can be stored in the list.
    uint16_t numElementsIn;        ///< Number of elements stored in the list.
    bool hasToEnableInterrupts;    ///< It is used to make this list thread safe. Internal usage only.
} list_t;

extern void listInit(list_t* self, uint16_t setMaxNumberOfElements, uint8_t setItemSize, uint8_t* setStorage);

/**
 * @brief Add a new element to the list by copying it.
 * @param self is a pointer to some list instance.
 * @param newElement is a pointer to the data that will copied into the list.
 * @return true if the element was added to the list.
 */
extern bool listAdd(list_t* self, const void* newElement);

/**
 * @brief Reserve memory for one item and receive a pointer to that memory.
 *
 * When handling with very big objects, it may be faster to reserve memory for the object and get a pointer to
 * copy data directly into the list element, i.e., avoiding copy data into local memory and then copying it to the list.
 * This also may be useful if dma requests are being used to fulfill the data object, i.e., the dma can write directly
 * to the list item reserved memory.
 *
 * @param self is a pointer to some list instance.
 * @return a valid pointer if the list was not full and NULL if list was full.
 */
extern void* listReserve(list_t* self);

/**
 * @brief Copy the oldest element of the list to destination. This method will not remove the element from the list.
 * @param self is a pointer to some list instance.
 * @param index is the index of the element that will be copied from the list.
 * @param dest is where to put the data copied from the list.
 * @return true if an element was pulled from the list.
 */
extern bool listGet(list_t* self, uint16_t index, void* dest);

/**
 * @brief Get a pointer to the oldest element in the list. No data will be copied.
 *
 * The user should test if the list is empty before using this function. In order to obtain greater performance, this
 * routine will not check if the list is empty.
 *
 * @param self is a pointer to some list instance.
 * @param index is the index of the element that will be copied from the list.
 * @return a pointer to the oldest element in the list.
 */
extern void* listPeek(list_t* self, uint16_t index);

/**
 * @brief Removes the oldest element in the list. It is safe to call it even if the list is empty.
 * @param self is a pointer to some list instance.
 */
extern bool listRemove(list_t* self, uint16_t index);

extern bool listFind(list_t* self, void* obj, uint16_t* index, uint16_t firstIndex);

extern bool listFindFromTheBegining(list_t* self, void* obj, uint16_t* index);

/**
 * @brief Clear the list, i.e., make it empty.
 * @param self is a pointer to some list instance.
 */
extern void listClear(list_t* self);

/**
 * @brief Determines if the list is empty.
 * @param self is a pointer to some list instance.
 * @return true if the list is empty.
 */
extern bool listIsEmpty(list_t* self);

/**
 * @brief Determines if the list is not empty.
 * @param self is a pointer to some list instance.
 * @return true if the list is not empty.
 */
extern bool listIsNotEmpty(list_t* self);

/**
 * @brief Determines if the list is full.
 * @param self is a pointer to some list instance.
 * @return true if the list is full.
 */
extern bool listIsFull(list_t* self);

/**
 * @brief Determines if the list is not full.
 * @param self is a pointer to some list instance.
 * @return true if the list is not full.
 */
extern bool listIsNotFull(list_t* self);

/**
 * Determines the number of elements in the list.
 * @param self is a pointer to some list instance.
 * @return the number of elements in the list.
 */
extern uint16_t listCount(list_t* self);

#ifdef __cplusplus
}
#endif

#endif //UTILS_LIST_H
