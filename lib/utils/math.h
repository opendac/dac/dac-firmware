#ifndef UTILS_MATH_H
#define UTILS_MATH_H

#include "lib/utils/types.h"

#define mathBetween(value, min, max)               ((value >= min) && (value <= max))
#define mathIsNegative(value)                      (value < 0)
#define mathIsPositive(value)                      (value > 0)
#define mathSign(value)                            (2 * mathIsPositive(value) - 1)
#define mathAbs(value)                             (mathSign(value) * value)
#define mathMinSigned(x1, x2)                      (((x1 + x2) / 2) - (mathAbs(x1 - x2) / 2))
#define mathMinUnsigned(x1, x2)                    (x1 > x2 ? x2 : x1)
#define mathMaxSigned(x1, x2)                      (((x1 + x2) / 2) + (mathAbs(x1 - x2) / 2))
#define mathMaxUnsigned(x1, x2)                    (x1 > x2 ? x1 : x2)
#define mathSaturateSigned(value, lower, upper)    (mathMinSigned(mathMaxSigned(value, upper), lower))
#define mathSaturateUnsigned(value, lower, upper)  (mathMinUnsigned(mathMaxUnsigned(value, upper), lower))

extern float mathQuantizer(float number, float base);

/**
 * @brief Gray codes.
 *
 * | Name             | Decimal | Binary | Gray |
 * | :--------------- | :-----: | :----: | :--: |
 * | GrayCode::GRAY_0 | 0       | 00     | 00   |
 * | GrayCode::GRAY_1 | 1       | 01     | 01   |
 * | GrayCode::GRAY_2 | 2       | 10     | 11   |
 * | GrayCode::GRAY_3 | 3       | 11     | 10   |
 *
 * The herein Gray codes assume the pins as (CHB, CHA).
 *
 * @note
 * If CHB is 1 and CHA is 0, then the Gray code is encoder::GrayCode::GRAY_3.
 */
typedef enum GrayCode {
    GrayCode0 = 0b00,              ///< Code 0.
    GrayCode1 = 0b01,              ///< Code 1.
    GrayCode2 = 0b11,              ///< Code 2.
    GrayCode3 = 0b10,              ///< Code 3.
} GrayCode;

#endif //UTILS_MATH_H
