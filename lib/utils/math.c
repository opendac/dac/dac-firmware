#include "math.h"

float mathQuantizer(float number, float base)
{
    const int32_t normalized_value = (int32_t) (number / base);
    return ((float) normalized_value) * base;
}
