#ifndef UTILS_FIFO_H
#define UTILS_FIFO_H

/**
 ***********************************************************************************************************************
 * @file    fifo.h
 * @author  José Roberto Colombo Junior
 * @brief   Header file of the fifo API.
 ***********************************************************************************************************************
 *
 * This library adds support to First-In First-Out (FIFO) queue. This implementation is supposed to be thread safe.
 *
 * @attention
 *
 * This is free software licensed under GPL.
 *
 ***********************************************************************************************************************
 */

#include "lib/utils/types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief The fifo description struct. You should instantiate one struct for each fifo.
 */
typedef struct fifo {
    uint8_t* storage;              ///< A pointer to memory that will be used exclusively by the fifo. It must have at least maxNumberOfElements * itemSize bytes.
    uint8_t itemSize;              ///< The size of each element that will be stored in the fifo.
    uint16_t maxNumberOfElements;  ///< The maximum number of elements that can be stored in the fifo.
    uint16_t numElementsIn;        ///< Number of elements stored in the fifo.
    uint16_t oldest;               ///< Index of the oldest element stored in the fifo.
    uint16_t newest;               ///< Index of the newest element stored in the fifo.
} fifo;

/**
 * @brief This functions is used to initialize a fifo and must be called before any usage of the fifo instance.
 * @param self is a pointer to some fifo instance.
 * @param setMaxNumberOfElements is the max number of elements stored in the fifo. It must be compatible with the size of storage.
 * @param setItemSize is the size of the item that will be stored in the fifo. Usually obtained with help of sizeof() macro.
 * @param storage is a pointer to memory that will be used exclusively by the fifo.
 */
extern void fifoInit(fifo* self, uint16_t setMaxNumberOfElements, uint8_t setItemSize, uint8_t* storage);

/**
 * @brief Add a new element to the fifo by copying it.
 * @param self is a pointer to some fifo instance.
 * @param newElement is a pointer to the data that will copied into the fifo.
 * @return true if the element was added to the fifo.
 */
extern bool fifoAdd(fifo* self, const void* newElement);

/**
 * @brief Reserve memory for one item and receive a pointer to that memory.
 *
 * When handling with very big objects, then it may be faster to reserve memory for the object and get a pointer to
 * copy data directly into the fifo element, i.e., avoiding copy data into local memory and then copying it to the fifo.
 * This also may be useful if dma requests are being used to fulfill the data object, i.e., the dma can write directly
 * to the list item reserved memory.
 *
 * @param self is a pointer to some fifo instance.
 * @return a valid pointer if the fifo was not full and NULL if fifo was full.
 */
extern void* fifoReserve(fifo* self);

/**
 * @brief Copy the oldest element of the list to destination and then remove it from the list.
 * @param self is a pointer to some fifo instance.
 * @param dest is where to put the data copied from the fifo.
 * @return true if an element was pulled from the fifo.
 */
extern bool fifoPull(fifo* self, void* dest);

/**
 * @brief Get a pointer to the oldest element in the fifo. No data will be copied.
 *
 * The user should test if the fifo is empty before using this function. In order to obtain greater performance, this
 * routine will not check if the fifo is empty.
 *
 * @param self is a pointer to some fifo instance.
 * @return a pointer to the oldest element in the fifo.
 */
extern void* fifoPeekOldest(fifo* self);

/**
 * @brief Removes the oldest element in the fifo. It is safe to call it even if the fifo is empty.
 * @param self is a pointer to some fifo instance.
 */
extern void fifoRemoveOldest(fifo* self);

/**
 * @brief Clear the fifo, i.e., make it empty.
 * @param self is a pointer to some fifo instance.
 */
extern void fifoClear(fifo* self);

/**
 * @brief Determines if the fifo is empty.
 * @param self is a pointer to some fifo instance.
 * @return true if the fifo is empty.
 */
extern bool fifoIsEmpty(fifo* self);

/**
 * @brief Determines if the fifo is not empty.
 * @param self is a pointer to some fifo instance.
 * @return true if the fifo is not empty.
 */
extern bool fifoIsNotEmpty(fifo* self);

/**
 * @brief Determines if the fifo is full.
 * @param self is a pointer to some fifo instance.
 * @return true if the fifo is full.
 */
extern bool fifoIsFull(fifo* self);

/**
 * @brief Determines if the fifo is not full.
 * @param self is a pointer to some fifo instance.
 * @return true if the fifo is not full.
 */
extern bool fifoIsNotFull(fifo* self);

/**
 * Determines the number of elements in the fifo.
 * @param self is a pointer to some fifo instance.
 * @return the number of elements in the fifo.
 */
extern size_t fifoCount(fifo* self);

#ifdef __cplusplus
}
#endif

#endif //UTILS_FIFO_H
