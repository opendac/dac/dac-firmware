#include "lib/utils/bit.h"

#if defined(__ARM_FEATURE_CLZ)
#include <cmsis_gcc.h>
#endif

uint8_t bitToNumber(uint32_t bit)
{
#if defined(__ARM_FEATURE_CLZ)
    const uint8_t number = 31 - __CLZ(bit);
#elif defined(__builtin_clz)
    const uint8_t number = 31 - __builtin_clz(bit);
#else
    uint8_t number = 0;
    while ((number < 32) && ((bit & numberToBit(number)) == 0))
    {
        number = number + 1;
    }
#endif
    return number;
}
