#include "SPI.h"
#include "lib/utils/compatibility/arduino.h"

#if defined(STM32F1)
#include <Drivers/STM32F1xx_HAL_Driver/Inc/stm32f1xx_hal_rcc.h>
#endif

HardwareSPI::HardwareSPI()
{
    _hspi = nullptr;
}

void HardwareSPI::begin()
{
    HAL_SPI_Init(_hspi);
}

void HardwareSPI::begin(SPI_HandleTypeDef* hspi)
{
    _hspi = hspi;
}

uint8_t HardwareSPI::transfer(uint8_t data)
{
    uint8_t rx_data;
    HAL_SPI_TransmitReceive(_hspi, &data, &rx_data, 1, HAL_MAX_DELAY);
    return rx_data;
}

uint16_t HardwareSPI::transfer16(uint16_t data)
{
    uint8_t rxData[2], txData[2];
    txData[1] = highByte(data);
    txData[0] = lowByte(data);
    HAL_SPI_TransmitReceive(_hspi, txData, rxData, 2, HAL_MAX_DELAY);
    data = (rxData[1] << 8) | rxData[0];
    return data;
}

void HardwareSPI::transfer(void *buf, size_t count)
{
    HAL_SPI_Transmit(_hspi, (uint8_t*) buf, count, HAL_MAX_DELAY);
}

void HardwareSPI::beginTransaction(SPISettings settings)
{
    if (_hspi->State == HAL_SPI_STATE_READY)
    {
        setBitOrder(settings.bitOrder);
        setClockFreq(settings.clockFreq);
        setMode(settings.dataMode);
    }
}

void HardwareSPI::endTransaction()
{
    // ST CubeHAL already takes care of stopping the SPI peripheral
}

void HardwareSPI::setBitOrder(BitOrder newBitOrder)
{
    if (newBitOrder == MSBFIRST)
    {
        _hspi->Instance->CR1 &= ~SPI_CR1_LSBFIRST_Msk;
    }
    else
    {
        _hspi->Instance->CR1 |= SPI_CR1_LSBFIRST_Msk;
    }
}

void HardwareSPI::setClockFreq(uint32_t newClockFreq)
{
    const uint32_t spiClock = HAL_RCC_GetSysClockFreq();
    const uint16_t div[] = {2, 4, 8, 16, 32, 64, 128, 256};
    uint8_t selected = 0;
    while (
            ( (spiClock / div[selected]) > newClockFreq )
            &&
            ( selected < (sizeof(div)/sizeof(uint16_t)) )
            )
    {
        selected++;
    }
    _hspi->Instance->CR1 &= ~(SPI_CR1_BR_Msk);
    _hspi->Instance->CR1 |= selected << SPI_CR1_BR_Pos;
}

void HardwareSPI::setMode(SPIMode newMode)
{
    _hspi->Instance->CR1 &= (~SPI_CR1_CPOL_Msk | SPI_CR1_CPHA_Msk);
    _hspi->Instance->CR1 |= newMode << SPI_CR1_CPHA_Pos;
}

HardwareSPI SPI;
