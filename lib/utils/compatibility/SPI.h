#ifndef UTILS_COMPATIBILITY_SPI_H
#define UTILS_COMPATIBILITY_SPI_H

#include "lib/architecture/spi.h"

#ifdef HAL_SPI_MODULE_ENABLED

typedef enum {
    SPI_MODE0 = 0,
    SPI_MODE1 = 1,
    SPI_MODE2 = 2,
    SPI_MODE3 = 3,
} SPIMode;

typedef enum BitOrder {
    LSBFIRST = 0,
    MSBFIRST
} BitOrder;

typedef struct SPISettings {
    SPISettings(uint32_t setClockFreq, BitOrder setBitOrder, SPIMode setDataMode) : clockFreq(setClockFreq), bitOrder(setBitOrder), dataMode(setDataMode) {};
    uint32_t clockFreq;
    BitOrder bitOrder;
    SPIMode dataMode;
} SPISettings;

class HardwareSPI {
public:
    HardwareSPI();
    ~HardwareSPI() = default;

    uint8_t transfer(uint8_t data);
    uint16_t transfer16(uint16_t data);
    void transfer(void *buf, size_t count);

    void beginTransaction(SPISettings settings);
    void endTransaction();

    void begin();
    void begin(SPI_HandleTypeDef* hspi);

private:
    SPI_HandleTypeDef* _hspi;
    void setBitOrder(BitOrder newBitOrder);
    void setClockFreq(uint32_t newClockFreq);
    void setMode(SPIMode newMode);
};

extern HardwareSPI SPI;

#endif

#endif //UTILS_COMPATIBILITY_SPI_H
