#ifndef COMPATIBILITY_ARDUINO_H
#define COMPATIBILITY_ARDUINO_H

#include "lib/utils/types.h"
#include "lib/utils/time.h"
#include "lib/architecture/gpio.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifndef LOW
#define LOW      0
#endif

#ifndef HIGH
#define HIGH     1
#endif

#ifndef OUTPUT
#define OUTPUT   GPIO_MODE_OUTPUT_PP
#endif

#ifndef INPUT
#define INPUT    GPIO_MODE_INPUT
#endif

#ifndef pgm_read_byte
#define pgm_read_byte(addr) (*(const unsigned char*)(addr))
#endif

#if !defined(pgm_read_word)
#define pgm_read_word(p) (*(p))
#endif

#if !defined(pgm_read_ptr)
#define pgm_read_ptr(p)  (*(p))
#endif

#if !defined(lowByte)
#define lowByte(w) ((uint8_t) ((w) & 0xFF))
#endif

#if !defined(highByte)
#define highByte(w) ( (uint8_t) (((w) >> 8) & 0xFF) )
#endif

extern void noInterrupts();

extern void interrupts();

#define delay(msecs)              delayMs(msecs)

#define delayMicroseconds(usecs)  delayUs(usecs)

#define micros()                  getTimeUs()

#define milis()                   getTimeMs()

typedef enum arduinoPin {
    ARDUINO_PA0 = 0,
    ARDUINO_PA1,
    ARDUINO_PA2,
    ARDUINO_PA3,
    ARDUINO_PA4,
    ARDUINO_PA5,
    ARDUINO_PA6,
    ARDUINO_PA7,
    ARDUINO_PA8,
    ARDUINO_PA9,
    ARDUINO_PA10,
    ARDUINO_PA11,
    ARDUINO_PA12,
    ARDUINO_PA13,
    ARDUINO_PA14,
    ARDUINO_PA15,
    ARDUINO_PB0,
    ARDUINO_PB1,
    ARDUINO_PB2,
    ARDUINO_PB3,
    ARDUINO_PB4,
    ARDUINO_PB5,
    ARDUINO_PB6,
    ARDUINO_PB7,
    ARDUINO_PB8,
    ARDUINO_PB9,
    ARDUINO_PB10,
    ARDUINO_PB11,
    ARDUINO_PB12,
    ARDUINO_PB13,
    ARDUINO_PB14,
    ARDUINO_PB15,
    ARDUINO_PC0,
    ARDUINO_PC1,
    ARDUINO_PC2,
    ARDUINO_PC3,
    ARDUINO_PC4,
    ARDUINO_PC5,
    ARDUINO_PC6,
    ARDUINO_PC7,
    ARDUINO_PC8,
    ARDUINO_PC9,
    ARDUINO_PC10,
    ARDUINO_PC11,
    ARDUINO_PC12,
    ARDUINO_PC13,
    ARDUINO_PC14,
    ARDUINO_PC15,
    ARDUINO_PD0,
    ARDUINO_PD1,
    ARDUINO_PD2,
    ARDUINO_PD3,
    ARDUINO_PD4,
    ARDUINO_PD5,
    ARDUINO_PD6,
    ARDUINO_PD7,
    ARDUINO_PD8,
    ARDUINO_PD9,
    ARDUINO_PD10,
    ARDUINO_PD11,
    ARDUINO_PD12,
    ARDUINO_PD13,
    ARDUINO_PD14,
    ARDUINO_PD15,
    ARDUINO_PE0,
    ARDUINO_PE1,
    ARDUINO_PE2,
    ARDUINO_PE3,
    ARDUINO_PE4,
    ARDUINO_PE5,
    ARDUINO_PE6,
    ARDUINO_PE7,
    ARDUINO_PE8,
    ARDUINO_PE9,
    ARDUINO_PE10,
    ARDUINO_PE11,
    ARDUINO_PE12,
    ARDUINO_PE13,
    ARDUINO_PE14,
    ARDUINO_PE15,
    ARDUINO_PF0,
    ARDUINO_PF1,
    ARDUINO_PF2,
    ARDUINO_PF3,
    ARDUINO_PF4,
    ARDUINO_PF5,
    ARDUINO_PF6,
    ARDUINO_PF7,
    ARDUINO_PF8,
    ARDUINO_PF9,
    ARDUINO_PF10,
    ARDUINO_PF11,
    ARDUINO_PF12,
    ARDUINO_PF13,
    ARDUINO_PF14,
    ARDUINO_PF15,
    ARDUINO_PG0,
    ARDUINO_PG1,
    ARDUINO_PG2,
    ARDUINO_PG3,
    ARDUINO_PG4,
    ARDUINO_PG5,
    ARDUINO_PG6,
    ARDUINO_PG7,
    ARDUINO_PG8,
    ARDUINO_PG9,
    ARDUINO_PG10,
    ARDUINO_PG11,
    ARDUINO_PG12,
    ARDUINO_PG13,
    ARDUINO_PG14,
    ARDUINO_PG15,
} arduinoPin;

extern void pinMode(uint8_t pin, uint32_t direction);

extern void digitalWrite(uint8_t pin, uint8_t value);

#ifdef __cplusplus
}
#endif

#endif //COMPATIBILITY_ARDUINO_H
