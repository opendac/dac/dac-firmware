#ifndef UTIL_TYPES_H_
#define UTIL_TYPES_H_

/**
 ***********************************************************************************************************************
 * @file    types.h
 * @author  José Roberto Colombo Junior
 * @brief   Header file of types traits.
 ***********************************************************************************************************************
 * @attention This is free software licensed under GPL.
 ***********************************************************************************************************************
 */

#include <stddef.h>
#include <stdint.h>
#ifndef __cplusplus
#include <stdbool.h>
#endif

/**
 * @def AS_U8(x)
 * @brief Statically casts a given number as uint8_t.
 * @param x is the given number.
 * @return the given number casted to uint8_t.
 */
#define AS_U8(x)  ((uint8_t) x)

/**
 * @def AS_U16(x)
 * @brief Statically casts a given number as uint16_t.
 * @param x is the given number.
 * @return the given number casted to uint16_t.
 */
#define AS_U16(x) ((uint16_t) x)

/**
 * @def AS_U32(x)
 * @brief Statically casts a given number as uint32_t.
 * @param x is the given number.
 * @return the given number casted to uint32_t.
 */
#define AS_U32(x) ((uint32_t) x)

#endif /* UTIL_TYPES_H_ */
