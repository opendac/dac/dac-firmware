#include "time.h"

uint32_t getTimeMs()
{
    return HAL_GetTick();
}

/*
 * This code is based on the website:
 * "a smarter way to write micros()" - http://micromouseusa.com/?p=296
 */
uint32_t getTimeUs()
{
#if defined(TIME_USING_SYSTICK)
    return (1000 * getTimeMs() + 1000) - (SysTick->VAL / (SystemCoreClock / 1000000));
#else
    return (1000 * getTimeMs() + 1000) - (TIME_TIMER_OBJECT->CNT / (TIME_TIMER_FREQUENCY / 1000000));
#endif
}

void delayMs(uint32_t msecs)
{
    delayUs(1000 * msecs);
}

void delayUs(uint32_t usecs)
{
    const uint32_t blocked_until = getTimeUs() + usecs;
    uint32_t count = 4 * usecs;
    while ((blocked_until > getTimeUs()) && (count > 0))
    {
        count = count - 1;
    }
}
