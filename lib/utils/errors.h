#ifndef UTILS_ERRORS_H
#define UTILS_ERRORS_H

#define debugStopHereIf(test) if((test)){for(;;){}}

typedef enum error_t {
    errorOk = 0,
    errorChipNotFound,
    errorInitializationFailed,
    errorPhyFailed,
    errorWriteFailed,
} error_t;

#endif //UTILS_ERRORS_H
