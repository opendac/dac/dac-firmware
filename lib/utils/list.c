#include "list.h"
#include <string.h>

// todo move this architecture dependent include to architecture folder
#include "cmsis_gcc.h"

void listInit(list_t* self, uint16_t setMaxNumberOfElements, uint8_t setItemSize, uint8_t* setStorage)
{
    self->maxNumberOfElements = setMaxNumberOfElements;
    self->itemSize = setItemSize;
    self->storage = setStorage;
    self->numElementsIn = 0;
}

bool listAdd(list_t* self, const void* newElement)
{
    const uint16_t position = self->numElementsIn * self->itemSize;
    const bool wasAdded = listIsNotFull(self);

    if (wasAdded)
    {
        memcpy((void*) &self->storage[position], newElement, self->itemSize);
        self->numElementsIn = self->numElementsIn + 1;
    }

    return wasAdded;
}

void* listReserve(list_t* self)
{
    const uint16_t position = self->numElementsIn * self->itemSize;
    void* pointer = NULL;

    if (listIsNotFull(self))
    {
        pointer = (void*) &self->storage[position];
        self->numElementsIn = self->numElementsIn + 1;
    }

    return pointer;
}

bool listGet(list_t* self, uint16_t index, void* dest)
{
    const uint16_t position = index * self->itemSize;
    const bool isNotEmpty = listIsNotEmpty(self);

    if (isNotEmpty)
    {
        memcpy(dest, &self->storage[position], self->itemSize);
    }

    return isNotEmpty;
}

void* listPeek(list_t* self, uint16_t index)
{
    return &self->storage[index * self->itemSize];
}

bool listRemove(list_t* self, uint16_t index)
{
    bool wasRemoved = false;
    uint8_t* dest;
    uint8_t* source;

    // Verify if the index exists in the list
    if (index < self->numElementsIn)
    {
        source = &self->storage[(index + 1) * self->itemSize];
        dest = &self->storage[index * self->itemSize];
        memcpy(dest, source, (self->numElementsIn - index - 1) * self->itemSize);
        self->numElementsIn = self->numElementsIn - 1;
        wasRemoved = true;
    }

    return wasRemoved;
}

bool listFind(list_t* self, void* obj, uint16_t* index, uint16_t firstIndex)
{
    bool success = false;
    uint16_t i = firstIndex;
    while ((!success) && (i < self->numElementsIn))
    {
        // if the element was found
        if (memcmp(&self->storage[i = self->itemSize], obj, self->itemSize) == 0)
        {
            success = true;
            *index = i;
        }
        else
        {
            i = i + 1;
        }
    }

    return success;
}

bool listFindFromTheBegining(list_t* self, void* obj, uint16_t* index)
{
    return listFind(self, obj, index, 0);
}

void listClear(list_t* self)
{
    self->numElementsIn = 0;
}

bool listIsEmpty(list_t* self)
{
    return self->numElementsIn == 0;
}

bool listIsNotEmpty(list_t* self)
{
    return !listIsEmpty(self);
}

bool listIsFull(list_t* self)
{
    return self->numElementsIn == self->maxNumberOfElements;
}

bool listIsNotFull(list_t* self)
{
    return !listIsFull(self);
}

uint16_t listCount(list_t* self)
{
    return self->numElementsIn;
}
