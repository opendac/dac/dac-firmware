#ifndef UTIL_TIME_H_
#define UTIL_TIME_H_

/**
 ***********************************************************************************************************************
 * @file    time.h
 * @author  José Roberto Colombo Junior
 * @brief   Header file of the time API.
 ***********************************************************************************************************************
 *
 * This library adds support to basic API relating time.
 *
 * @attention
 *
 * This is free software licensed under GPL.
 *
 ***********************************************************************************************************************
 */

#include "lib/utils/types.h"
#include "lib/architecture/timer.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Define here the time source. In STM32 application without FreeRTOS the SysTick is a good source. If you are using a
 * timer as time source, then comment out the following line. If the following line is enabled, then TIME_TIMER_OBJECT
 * and TIME_TIMER_FREQUENCY have no effect.
 */
#define TIME_USING_SYSTICK

/**
 * Define here the time object. For example, if you are using SysTick, then set it to SysTick. If you are using Timer 1,
 * then set it to TIM1.
 */
#define TIME_TIMER_OBJECT TIM1

/**
 * Define here the timer frequency.
 */
#define TIME_TIMER_FREQUENCY 72000000

/**
 * Get current time (in milliseconds) since system initialization.
 * @return current time in milliseconds.
 */
uint32_t getTimeMs();

/**
 * Get current time (in microseconds) since system initialization.
 * @return current time in microseconds.
 */
uint32_t getTimeUs();

/**
 * This function blocks the CPU during time_to_wait milliseconds.
 * @param msecs is the blocking time in milliseconds.
 */
void delayMs(uint32_t msecs);

/**
 * This function blocks the CPU during time_to_wait microseconds.
 * @param usecs is the blocking time in microseconds.
 */
void delayUs(uint32_t usecs);

#ifdef __cplusplus
}
#endif

#endif /* UTIL_TIME_H_ */
