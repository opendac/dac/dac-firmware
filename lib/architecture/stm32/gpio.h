#ifndef ARCHITECTURE_STM32_GPIO_H_
#define ARCHITECTURE_STM32_GPIO_H_

/**
 ***********************************************************************************************************************
 * @file    gpio.h
 * @author  José Roberto Colombo Junior
 * @brief   Header file of the basic gpio driver.
 ***********************************************************************************************************************
 *
 * This library adds support to the microcontroller basic digital gpio. It tries to be the fastest as possible, avoiding
 * all if else statements possible.
 *
 * @note The pin number notation is used here because it is easy to obtain the pin position from the number, for example
 *       by doing (1 << pinNumber).
 *
 * @attention
 *
 * This is free software licensed under GPL.
 *
 ***********************************************************************************************************************
 */

#include "lib/architecture/detect.h"
#include "lib/utils/types.h"
#include "lib/utils/bit.h"

#if defined(STM32F1)
#include "stm32f1xx_hal.h"
#include "stm32f1xx_hal_gpio.h"
#include "stm32f1xx_hal_def.h"
#elif defined(STM32F4)
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_gpio.h"
#include "stm32f4xx_hal_def.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Defines the gpio port handler.
 * @warning This is architecture dependent.
 */
typedef GPIO_TypeDef gpioPort;

/**
 * @brief Get the associated IRQ to a pin number.
 * @param pinNumber is the pin number.
 * @return the IRQ associated with that pin number.
 */
extern IRQn_Type gpioGetIrq(uint8_t pinNumber);

/**
 * @def gpioRead(port, pinPosition)
 * @brief Read the pin logic state.
 * @param port is a pointer to the pin port.
 * @param pinPosition is the pin position. Example: the pin number 3 has pin position 0b1000.
 * @return the logic state of the pin, 1 or 0.
 */
#define gpioFastRead(port, pinPosition)          AS_U16((port->IDR & pinPosition)!=0)

/**
 * @def gpioWrite(port, pinPosition, level)
 * @brief Updates the pin output logic state.
 * @param port is a pointer to the pin port.
 * @param pinPosition is the pin position. Example: the pin number 3 has pin position 0b1000.
 * @param level the new logic state of the pin. It can 1 or 0.
 */
#define gpioFastWrite(port, pinPosition, level)  port->BSRR=(pinPosition<<(16*(1-level)))

#ifdef __cplusplus
}
#endif

#endif /* ARCHITECTURE_STM32_GPIO_H_ */
