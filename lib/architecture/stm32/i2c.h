#ifndef ARCHITECTURE_STM32_I2C_H
#define ARCHITECTURE_STM32_I2C_H

#ifdef STM32F1
#include "stm32f1xx_hal.h"
#include "stm32f1xx_hal_i2c.h"
#include "stm32f1xx_hal_def.h"
#endif

#endif //ARCHITECTURE_STM32_I2C_H
