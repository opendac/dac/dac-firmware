#include "lib/architecture/stm32/gpio.h"
#include "lib/utils/bit.h"

static const IRQn_Type gpioIrqTable[] = {
        EXTI0_IRQn,
        EXTI1_IRQn,
        EXTI2_IRQn,
        EXTI3_IRQn,
        EXTI4_IRQn,
        EXTI9_5_IRQn,
        EXTI9_5_IRQn,
        EXTI9_5_IRQn,
        EXTI9_5_IRQn,
        EXTI9_5_IRQn,
        EXTI15_10_IRQn,
        EXTI15_10_IRQn,
        EXTI15_10_IRQn,
        EXTI15_10_IRQn,
        EXTI15_10_IRQn,
        EXTI15_10_IRQn,
};

IRQn_Type gpioGetIrq(uint8_t pinNumber)
{
    const bool safe = pinNumber < (sizeof(gpioIrqTable) / sizeof(IRQn_Type));
    const IRQn_Type irq = (safe) ? gpioIrqTable[pinNumber] : EXTI0_IRQn;
    return irq;
}
