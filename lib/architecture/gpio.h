#ifndef ARCHITECTURE_GPIO_H_
#define ARCHITECTURE_GPIO_H_

#include "detect.h"

#if defined(STM32F1)
    #include "lib/architecture/stm32/gpio.h"
#endif

#endif /* ARCHITECTURE_GPIO_H_ */
