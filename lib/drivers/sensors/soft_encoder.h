#ifndef DRIVERS_SENSORS_SOFT_ENCODER_H
#define DRIVERS_SENSORS_SOFT_ENCODER_H

/**
 ***********************************************************************************************************************
 * @file    encoder.h
 * @author  José Roberto Colombo Junior
 * @brief   Header file of the encoder driver.
 ***********************************************************************************************************************
 *
 * This library adds support for reading encoders with software emulation. It is supposed that the events will be
 * captured using digital pin interrupts. Some modern microcontrollers have timers capable of reading encoder counts.
 * This is not the case of this library.
 *
 * This library was carefully writen to be the fastest possible. The real time operation requires only a switch
 * statement and a add operation.
 *
 * @attention
 *
 * This is free software licensed under GPL.
 *
 ***********************************************************************************************************************
 */

#include "lib/architecture/gpio.h"
#include "lib/utils/math.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Supported quadrature modes.
 *
 * This option applies only for counting mode.
 * In order to avoid encoder phase problems, the time measurement occurs
 * only with rising edges of CHA.
 */
typedef enum softEncoderQuadrature {
    softEncoderQuadratureNone = 0,           ///< This option is used to disable the encoder and it is not available in the computer interface.
    softEncoderQuadratureX1 = 1,             ///< Capture CHA rising edges.
    softEncoderQuadratureX2 = 2,             ///< Capture CHA rising and falling edges.
    softEncoderQuadratureX4 = 3,             ///< Capture all edges on CHA and CHB.
} softEncoderQuadrature;

/**
 * A struct describing a software implemented encoder.
 */
typedef struct softEncoder {
    uint8_t irqPriority;                     ///< It is recommended to use maximum priority to encoder events.
    uint8_t transitionConfig;                ///< Transition configuration (holds actual quadrature and time measurement enable). The idea is to use this variable as initializer to the transition calculation, making the processing faster.
    int8_t cwIncrement;                      ///< The clockwise increment is +1 or -1?
    uint8_t actualCode;                      ///< Actual gray state.
    uint16_t chaPinPosition;                 ///< CHA pin position.
    uint16_t chbPinPosition;                 ///< CHB pin position.
    gpioPort *chaPort;                       ///< CHA gpio handler.
    gpioPort *chbPort;                       ///< CHB gpio handler.
    int16_t count;                           ///< Pulses count.

#ifdef ENCODER_ERROR_SUPPORT
    uint32_t error_count;                    ///< This variable is increased by 1 unit every time an invalid state is detected.
#endif
} softEncoder;


/**
 * @name Initialization.
 *
 *  Methods that you should use to initialize the encoder driver.
 */
/**@{*/

/**
 * This method must be called before start using the encoder. It will
 * initialize internal variables and GPIO in reset state.
 * @param self
 * @param chaPinPosition
 * @param chaPort
 * @param chbPinPosition
 * @param chbPort
 * @param irqPriority
 */
extern void softEncoderBegin(softEncoder *self, uint16_t chaPinPosition, gpioPort *chaPort, uint16_t chbPinPosition,
                             gpioPort *chbPort, uint8_t irqPriority);

/**@}*/
/**
 * @name Configuration.
 *
 *  Methods that you should use to configure the encoder.
 */
/**@{*/

/**
 * Set the CW increment to +. This method has the same effect of swapping the encoder CHA and CHB cables.
 */
extern void softEncoderSetCW(softEncoder *self);

/**
 * Set the CW increment to -. This method has the same effect of swapping the encoder CHA and CHB cables.
 */
extern void softEncoderSetCCW(softEncoder *self);

/**
 * Return if the encoder is enabled.
 * @return true if enabled or false if disabled.
 */
extern bool softEncoderIsEnabled(softEncoder *self);

/**
 * This method will enable the encoder. The pin interrupts will be enabled accordingly to the configured quadrature.
 */
extern void softEncoderEnable(softEncoder *self);

/**
 * It disables the encoder by disabling the interrupts but the quadrature setting will be retained.
 */
extern void softEncoderDisable(softEncoder *self);

/**
 * This method updates the quadrature.
 *
 * @warning The method will not make any change to interrupts.
 *          It is required to disable and enable the encoder to the
 *          quadrature updates to be applied.
 *
 * @param newQuadrature is the desired quadrature (@see softEncoderQuadrature).
 */
extern void softEncoderSetQuadrature(softEncoder *self, softEncoderQuadrature newQuadrature);

/**@}*/
/**
 * @name Reading measurements.
 * Methods for reading and manipulating measurements.
 */
/**@{*/

/**
 * This method resets the counting.
 */
extern void softEncoderResetCounting(softEncoder *self);

/**
 * This method sets the counting to the desired value.
 */
extern void softEncoderSetCounting(softEncoder *self, int32_t value);

/**
 * Get the pulses count.
 *
 * @return a number representing pulses counting.
 */
extern int32_t softEncoderGetPulsesCount(softEncoder *self);

/**
 * This function processes the registered events. It is safe to be
 * called even if no events were registered. It must be added to a
 * main loop or a task.
 *
 * @warning Although it can be called from the ISR this is not
 *          recommended. This encoder driver has a queue to hold
 *          the encoder events when CPU is busy with more important
 *          tasks.
 *
 * @return the number of processed events.
 */
extern void softEncoderProcessEvent(softEncoder *self);

#ifdef ENCODER_ERROR_SUPPORT
/**
     * This method resets the error counting.
     */
    void reset_error_counting()
    {
        error_count = 0;
    }
#endif

/**@}*/

#ifdef __cplusplus
}
#endif

#endif //DRIVERS_SENSORS_SOFT_ENCODER_H
