#include "lib/drivers/sensors/soft_encoder.h"
#include "lib/utils/bit.h"

/**
 * @brief This transition table contains all the possible transitions between
 *        the encoder states. It is optimized to minimize adding if-else
 *        statements.
 *
 * The transition has the following format:
 *
 * 0b Q1 Q2 RESERVED RESERVED NGA NGB OGA OGB
 *    |___|                   |_____| |_____|
 *      |                        |       |
 *      |                        |       +-> Old gray encoder state in 1st and 2nd bits
 *      |                        +---------> New gray encoder state in 3th and 4th bits
 *      +----------------------------------> Quadrature in 8th and 7th bits
 */
typedef enum softEncoderTransition {
    softEncoderTransitionPosQuadrature  = 6,                                                                                     ///< Quadrature position.
    softEncoderTransitionPosNewGray     = 2,                                                                                     ///< Encoder new state (in gray) position.
    softEncoderTransitionPosOldGray     = 0,                                                                                     ///< Encoder old state (in gray) position.
    softEncoderTransitionX1             = softEncoderQuadratureX1 << softEncoderTransitionPosQuadrature,                         ///< Quadrature X1 in position. Internal usage, please do not change.
    softEncoderTransitionX2             = softEncoderQuadratureX2 << softEncoderTransitionPosQuadrature,                         ///< Quadrature X2 in position. Internal usage, please do not change.
    softEncoderTransitionX4             = softEncoderQuadratureX4 << softEncoderTransitionPosQuadrature,                         ///< Quadrature X4 in position. Internal usage, please do not change.
    softEncoderTransitionX1Gray0ToGray1 = softEncoderTransitionX1 | GrayCode0 | (GrayCode1 << softEncoderTransitionPosNewGray),  ///< Transition in X1 quadrature from gray 0 to gray 1.
    softEncoderTransitionX1Gray1ToGray1 = softEncoderTransitionX1 | GrayCode1 | (GrayCode1 << softEncoderTransitionPosNewGray),  ///< Transition in X1 quadrature from gray 1 to gray 1.
    softEncoderTransitionX1Gray2ToGray2 = softEncoderTransitionX1 | GrayCode2 | (GrayCode2 << softEncoderTransitionPosNewGray),  ///< Transition in X1 quadrature from gray 2 to gray 2.
    softEncoderTransitionX1Gray3ToGray2 = softEncoderTransitionX1 | GrayCode3 | (GrayCode2 << softEncoderTransitionPosNewGray),  ///< Transition in X1 quadrature from gray 3 to gray 2.
    softEncoderTransitionX2Gray0ToGray1 = softEncoderTransitionX2 | GrayCode0 | (GrayCode1 << softEncoderTransitionPosNewGray),  ///< Transition in X2 quadrature from gray 0 to gray 1.
    softEncoderTransitionX2Gray0ToGray2 = softEncoderTransitionX2 | GrayCode0 | (GrayCode2 << softEncoderTransitionPosNewGray),  ///< Transition in X2 quadrature from gray 0 to gray 2.
    softEncoderTransitionX2Gray1ToGray0 = softEncoderTransitionX2 | GrayCode1 | (GrayCode0 << softEncoderTransitionPosNewGray),  ///< Transition in X2 quadrature from gray 1 to gray 0.
    softEncoderTransitionX2Gray1ToGray3 = softEncoderTransitionX2 | GrayCode1 | (GrayCode3 << softEncoderTransitionPosNewGray),  ///< Transition in X2 quadrature from gray 1 to gray 3.
    softEncoderTransitionX2Gray2ToGray0 = softEncoderTransitionX2 | GrayCode2 | (GrayCode0 << softEncoderTransitionPosNewGray),  ///< Transition in X2 quadrature from gray 2 to gray 0.
    softEncoderTransitionX2Gray2ToGray3 = softEncoderTransitionX2 | GrayCode2 | (GrayCode3 << softEncoderTransitionPosNewGray),  ///< Transition in X2 quadrature from gray 2 to gray 3.
    softEncoderTransitionX2Gray3ToGray1 = softEncoderTransitionX2 | GrayCode3 | (GrayCode1 << softEncoderTransitionPosNewGray),  ///< Transition in X2 quadrature from gray 3 to gray 1.
    softEncoderTransitionX2Gray3ToGray2 = softEncoderTransitionX2 | GrayCode3 | (GrayCode2 << softEncoderTransitionPosNewGray),  ///< Transition in X2 quadrature from gray 3 to gray 2.
    softEncoderTransitionX4Gray0ToGray1 = softEncoderTransitionX4 | GrayCode0 | (GrayCode1 << softEncoderTransitionPosNewGray),  ///< Transition in X4 quadrature from gray 0 to gray 1.
    softEncoderTransitionX4Gray0ToGray3 = softEncoderTransitionX4 | GrayCode0 | (GrayCode3 << softEncoderTransitionPosNewGray),  ///< Transition in X4 quadrature from gray 0 to gray 3.
    softEncoderTransitionX4Gray1ToGray0 = softEncoderTransitionX4 | GrayCode1 | (GrayCode0 << softEncoderTransitionPosNewGray),  ///< Transition in X4 quadrature from gray 1 to gray 0.
    softEncoderTransitionX4Gray1ToGray2 = softEncoderTransitionX4 | GrayCode1 | (GrayCode2 << softEncoderTransitionPosNewGray),  ///< Transition in X4 quadrature from gray 1 to gray 2.
    softEncoderTransitionX4Gray2ToGray1 = softEncoderTransitionX4 | GrayCode2 | (GrayCode1 << softEncoderTransitionPosNewGray),  ///< Transition in X4 quadrature from gray 2 to gray 1.
    softEncoderTransitionX4Gray2ToGray3 = softEncoderTransitionX4 | GrayCode2 | (GrayCode3 << softEncoderTransitionPosNewGray),  ///< Transition in X4 quadrature from gray 2 to gray 3.
    softEncoderTransitionX4Gray3ToGray0 = softEncoderTransitionX4 | GrayCode3 | (GrayCode0 << softEncoderTransitionPosNewGray),  ///< Transition in X4 quadrature from gray 3 to gray 0.
    softEncoderTransitionX4Gray3ToGray2 = softEncoderTransitionX4 | GrayCode3 | (GrayCode2 << softEncoderTransitionPosNewGray),  ///< Transition in X4 quadrature from gray 3 to gray 2.
} softEncoderTransition;

/**
 * This method is used to read the encoder pins.
 * @return the encoder actual digital state (chB, chA).
 */
static uint8_t softEncoderReadGpio(softEncoder* self)
{
    const GPIO_PinState chaState = HAL_GPIO_ReadPin(self->chaPort, self->chaPinPosition);
    const GPIO_PinState chbState = HAL_GPIO_ReadPin(self->chbPort, self->chbPinPosition);
    return (chbState << 1) | chaState;
}

/**
 * This method update the gpio modes accordingly to the new quadrature.
 * @param newQuadrature is the new quadrature.
 */
static void softEncoderUpdateGpioModes(softEncoder* self, softEncoderQuadrature newQuadrature)
{
    const uint8_t chaPinNumber = bitToNumber(self->chaPinPosition);
    const uint8_t chbPinNumber = bitToNumber(self->chbPinPosition);

    GPIO_InitTypeDef init;
    init.Speed = GPIO_SPEED_FREQ_HIGH;  // default configuration
    init.Pull = GPIO_PULLUP;            // default configuration

    init.Pin = self->chaPinPosition;
    switch (newQuadrature) {
        case softEncoderQuadratureNone:
            init.Mode = GPIO_MODE_INPUT;
            HAL_GPIO_Init(self->chaPort, &init);
            break;

        case softEncoderQuadratureX1:
        case softEncoderQuadratureX2:
            init.Mode = GPIO_MODE_IT_RISING;
            HAL_GPIO_Init(self->chaPort, &init);
            HAL_NVIC_SetPriority(gpioGetIrq(chaPinNumber), self->irqPriority, 0);
            HAL_NVIC_EnableIRQ(gpioGetIrq(chaPinNumber));
            break;

        case softEncoderQuadratureX4:
            init.Mode = GPIO_MODE_IT_RISING_FALLING;
            HAL_GPIO_Init(self->chaPort, &init);
            HAL_NVIC_SetPriority(gpioGetIrq(chaPinNumber), self->irqPriority, 0);
            HAL_NVIC_EnableIRQ(gpioGetIrq(chaPinNumber));
            break;
    }

    init.Pin = self->chbPinPosition;
    switch (newQuadrature) {
        case softEncoderQuadratureNone:
        case softEncoderQuadratureX1:
        case softEncoderQuadratureX2:
            init.Mode = GPIO_MODE_INPUT;
            HAL_GPIO_Init(self->chbPort, &init);
            break;

        case softEncoderQuadratureX4:
            HAL_GPIO_Init(self->chbPort, &init);
            HAL_NVIC_SetPriority(gpioGetIrq(chbPinNumber), self->irqPriority, 0);
            HAL_NVIC_EnableIRQ(gpioGetIrq(chbPinNumber));
            break;
    }
}

/**
 * This method is used to get the actual encoder quadrature.
 * @return the encoder actual quadrature.
 */
static softEncoderQuadrature softEncoderGetQuadrature(softEncoder* self)
{
    const uint8_t mask = 0b11;
    const uint8_t pos = softEncoderTransitionPosQuadrature;
    return bitRead(&self->transitionConfig, pos, (mask << pos));
}

void softEncoderBegin(softEncoder* self, uint16_t chaPinPosition, gpioPort* chaPort, uint16_t chbPinPosition, gpioPort* chbPort, uint8_t irqPriority)
{
    self->irqPriority = irqPriority;
    self->cwIncrement = 1;
    self->transitionConfig = 0;
    self->count = 0;

    // set both pins as input
    self->chaPinPosition = chaPinPosition;
    self->chaPort = chaPort;
    self->chaPinPosition = chbPinPosition;
    self->chbPort = chbPort;

    // initialize the gpio as input
    GPIO_InitTypeDef gpioInit = { .Pin = chaPinPosition, .Mode = GPIO_MODE_INPUT, .Pull = GPIO_PULLUP, .Speed = GPIO_SPEED_FREQ_HIGH};
    HAL_GPIO_Init(self->chaPort, &gpioInit);
    gpioInit.Pin = chbPinPosition;
    HAL_GPIO_Init(self->chbPort, &gpioInit);
    self->actualCode = (GrayCode) softEncoderReadGpio(self);
}

void softEncoderSetCW(softEncoder* self)
{
    self->cwIncrement = mathAbs(self->cwIncrement);
}

void softEncoderSetCCW(softEncoder* self)
{
    self->cwIncrement = (int8_t) -mathAbs(self->cwIncrement);
}

bool softEncoderIsEnabled(softEncoder* self)
{
    return softEncoderGetQuadrature(self);
}

void softEncoderEnable(softEncoder* self)
{
    const softEncoderQuadrature quadrature = softEncoderGetQuadrature(self);
    softEncoderUpdateGpioModes(self, quadrature);
}

void softEncoderDisable(softEncoder* self)
{
    softEncoderUpdateGpioModes(self, softEncoderQuadratureNone);
}

void softEncoderSetQuadrature(softEncoder* self, softEncoderQuadrature newQuadrature)
{
    // Update the transition config
    self->transitionConfig = (
            AS_U8(newQuadrature) << AS_U8(softEncoderTransitionPosQuadrature)
            |
            (self->actualCode << AS_U8(softEncoderTransitionPosOldGray))
    );

    // Also update the cw_increment
    switch (newQuadrature) {
        case softEncoderQuadratureNone:
        case softEncoderQuadratureX4:
            self->cwIncrement = mathSign(self->cwIncrement) * 1;
            break;
        case softEncoderQuadratureX2:
            self->cwIncrement = mathSign(self->cwIncrement) * 2;
            break;
        case softEncoderQuadratureX1:
            self->cwIncrement = mathSign(self->cwIncrement) * 4;
            break;
    }
}

void softEncoderSetCounting(softEncoder* self, int32_t value)
{
    self->count = value;
}

void softEncoderResetCounting(softEncoder* self)
{
    softEncoderSetCounting(self, 0);
}

int32_t softEncoderGetPulsesCount(softEncoder* self)
{
    return self->count;
}

void softEncoderProcessEvent(softEncoder* self)
{
    /*
     * Note: an interrupt only occurs if signal rising or falling is detected.
     *       Therefore, if an interrupt occurs the encoder has moved.
     *
     * X1 (count when CHA detect rising edges)
     *
     *         +-------+       +-------+      +-----+
     * CHA     |       |       |       |      |     |
     *      ---+       +-------+       +------+     +--------
     *
     *             +-------+       +-------+     +-----+
     * CHB         |       |       |       |     |     |
     *      -------+       +-------+       +-----+     +-----
     * GRAY  0 | 1 | 2 | 3 | 0 | 1 | 2 | 3
     *
     *         +-------+       +-------+      +-----+
     * CHA     |       |       |       |      |     |
     *      ---+       +-------+       +------+     +--------
     *
     *      -------+       +-------+       +-----+     +-----
     * CHB         |       |       |       |     |     |
     *             +-------+       +-------+     +-----+
     * GRAY  3 | 2 | 1 | 0 | 3 | 2 | 1 | 0
     *
     * forward sequence:
     *     GRAY0 -> GRAY1 (count +1)
     *     GRAY1 -> GRAY1 (count +4)
     * backward sequence:
     *     GRAY3 -> GRAY2 (count -1)
     *     GRAY2 -> GRAY2 (count -4)
     *
     **************************************************************
     *
     * X2 (count when CHA detect rising/falling edges)
     *
     *         +-------+       +-------+      +-----+
     * CHA     |       |       |       |      |     |
     *      ---+       +-------+       +------+     +--------
     *
     *             +-------+       +-------+     +-----+
     * CHB         |       |       |       |     |     |
     *      -------+       +-------+       +-----+     +-----
     * GRAY  0 | 1 | 2 | 3 | 0 | 1 | 2 | 3
     *
     *         +-------+       +-------+      +-----+
     * CHA     |       |       |       |      |     |
     *      ---+       +-------+       +------+     +--------
     *
     *      -------+       +-------+       +-----+     +-----
     * CHB         |       |       |       |     |     |
     *             +-------+       +-------+     +-----+
     * GRAY  3 | 2 | 1 | 0 | 3 | 2 | 1 | 0
     *
     * forward sequence:
     *     GRAY0 -> GRAY1 (count +1)
     *     GRAY1 -> GRAY3 (count +2)
     *     GRAY3 -> GRAY1 (count +2)
     *     or
     *     GRAY2 -> GRAY3 (count +1)
     * backward sequence:
     *     GRAY3 -> GRAY2 (count -1)
     *     GRAY2 -> GRAY0 (count -2)
     *     GRAY0 -> GRAY2 (count -2)
     *     or
     *     GRAY1 -> GRAY0 (count -1)
     *
     **************************************************************
     *
     * X4 (count when both channels detect rising/falling edges)
     *
     *         +-------+       +-------+      +-----+
     * CHA     |       |       |       |      |     |
     *      ---+       +-------+       +------+     +--------
     *
     *             +-------+       +-------+     +-----+
     * CHB         |       |       |       |     |     |
     *      -------+       +-------+       +-----+     +-----
     * GRAY  0 | 1 | 2 | 3 | 0 | 1 | 2 | 3
     *
     *         +-------+       +-------+      +-----+
     * CHA     |       |       |       |      |     |
     *      ---+       +-------+       +------+     +--------
     *
     *      -------+       +-------+       +-----+     +-----
     * CHB         |       |       |       |     |     |
     *             +-------+       +-------+     +-----+
     * GRAY  3 | 2 | 1 | 0 | 3 | 2 | 1 | 0
     *
     * forward sequence:
     *     GRAY3 -> GRAY0 -> GRAY1 -> GRAY2 -> GRAY3 (count +1)
     * backward sequence:
     *     GRAY2 -> GRAY1 -> GRAY0 -> GRAY3 -> GRAY2 (count -1)
     */

    const uint8_t pinState = softEncoderReadGpio(self);
    const softEncoderTransition transition = (softEncoderTransition) (
            self->transitionConfig
            |
            (pinState << AS_U8(softEncoderTransitionPosNewGray))
            |
            (self->actualCode << AS_U8(softEncoderTransitionPosOldGray))
    );
    self->actualCode = pinState;

    switch(transition) {
        // FORWARD +1
        case softEncoderTransitionX1Gray0ToGray1:
        case softEncoderTransitionX2Gray0ToGray1:
        case softEncoderTransitionX2Gray2ToGray3:
        case softEncoderTransitionX4Gray0ToGray1:
        case softEncoderTransitionX4Gray1ToGray2:
        case softEncoderTransitionX4Gray2ToGray3:
        case softEncoderTransitionX4Gray3ToGray0:
            // FORWARD +2
        case softEncoderTransitionX2Gray1ToGray3:
        case softEncoderTransitionX2Gray3ToGray1:
            // FORWARD +4
        case softEncoderTransitionX1Gray1ToGray1:
            self->count = self->count + self->cwIncrement;
            break;

            // BACKWARD -1
        case softEncoderTransitionX1Gray3ToGray2:
        case softEncoderTransitionX2Gray3ToGray2:
        case softEncoderTransitionX2Gray1ToGray0:
        case softEncoderTransitionX4Gray0ToGray3:
        case softEncoderTransitionX4Gray3ToGray2:
        case softEncoderTransitionX4Gray2ToGray1:
        case softEncoderTransitionX4Gray1ToGray0:
            // BACKWARD -2
        case softEncoderTransitionX2Gray0ToGray2:
        case softEncoderTransitionX2Gray2ToGray0:
            // BACKWARD -4
        case softEncoderTransitionX1Gray2ToGray2:
            self->count = self->count - self->cwIncrement;
            break;

        default:
            // The stopped transition does not count +1 or -1. So, it does not need to be implemented.
#ifdef ENCODER_ERROR_SUPPORT
            self->errorCount = self->errorCount + 1;
#endif
            break;
    }
}
