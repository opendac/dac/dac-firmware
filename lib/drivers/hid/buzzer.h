#ifndef DRIVERS_HID_BUZZER_H
#define DRIVERS_HID_BUZZER_H

#include "lib/drivers/hal/timer.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * This function will make the buzzer stop producing sounds (make it mute).
 * @param htim is the ST timer handler.
 * @param channel is the ST timer channel (TIM_CHANNEL_1, TIM_CHANNEL_2, etc).
 */
void buzzerMute(TIM_HandleTypeDef *htim, uint32_t channel);

/**
 * This function will make the buzzer produce sound at a specific frequency.
 * @param htim is the ST timer handler.
 * @param channel is the ST timer channel (TIM_CHANNEL_1, TIM_CHANNEL_2, etc).
 * @param frequencyHz is the desired frequency (in Hertz).
 */
void buzzerBeep(TIM_HandleTypeDef *htim, uint32_t channel, float frequencyHz);

#ifdef __cplusplus
}
#endif

#endif //DRIVERS_HID_BUZZER_H
