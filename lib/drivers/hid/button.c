#include "lib/drivers/hid/button.h"

static inline bool hidButtonIsPressed(hidButton_t* self)
{
    return (self->gpioRead() > 0) == self->logicStateWhenPressed;
}

static inline bool hidButtonIsNotPressed(hidButton_t* self)
{
    return !hidButtonIsPressed(self);
}

void hidButtonInit(hidButton_t* self, uint8_t (*gpioRead)())
{
    self->callbackPressed = NULL;
    self->callbackLongPressed = NULL;
    self->callbackFirstPressed = NULL;
    self->gpioRead = gpioRead;
    hidButtonSetDebounceTime(self, 10);
    hidButtonSetPressedTime(self, 150);
    hidButtonSetLongPressedTime(self, 1000);
    self->state = (hidButtonIsPressed(self)) ? hidButtonStateWaitingForPressedDebounceCompletion : hidButtonStateWaitingForReleaseDebounceCompletion;
}

void hidButtonSetPressedTime(hidButton_t* self, uint32_t msecs)
{
    self->pressedTimeMs = msecs;
}

void hidButtonSetLongPressedTime(hidButton_t* self, uint32_t msecs)
{
    self->longPressedTimeMs = msecs;
}

void hidButtonSetDebounceTime(hidButton_t* self, uint32_t msecs)
{
    self->debounceTimeMs = msecs;
}

void hidButtonAttachPressed(hidButton_t* self, void (*onPressed)())
{
    self->callbackPressed = onPressed;
}

void hidButtonAttachLongPressed(hidButton_t* self, void (*onLongPressed)())
{
    self->callbackLongPressed = onLongPressed;
}

void hidButtonAttachFirstPressed(hidButton_t* self, void (*onFirstPressed)())
{
    self->callbackFirstPressed = onFirstPressed;
}

void hidButtonRunPeriodically(hidButton_t* self)
{
    const uint32_t now = getTimeMs();
    const bool isPressed = hidButtonIsPressed(self);
    uint32_t elapsedTime;

    switch (self->state) {
        case hidButtonStateWaitingToBePressed:
            if (isPressed)
            {
                self->time = now;
                self->state = hidButtonStateWaitingForPressedDebounceCompletion;
                self->wasLongPressed = false;
            }
            break;

        case hidButtonStateWaitingForPressedDebounceCompletion:
            if (now > (self->time + self->debounceTimeMs))
            {
                self->state = hidButtonStateWaitingForReleaseTransition;
                if (self->callbackFirstPressed != NULL)
                {
                    self->callbackFirstPressed();
                }
            }
            break;

        case hidButtonStateWaitingForReleaseTransition:
            elapsedTime = now - self->time;

            // in this case the user is holding the button pressed
            if (isPressed)
            {
                if (elapsedTime > self->longPressedTimeMs)
                {
                    // update the time variable to prevent from undesired multiple calls to callbackLongPressed()
                    self->time = now;
                    self->wasLongPressed = true;
                    if (self->callbackLongPressed != NULL)
                    {
                        self->callbackLongPressed();
                    }
                }
            }
            // in this case the user has released the button
            else  // (!isPressed)
            {
                self->time = now;
                self->state = hidButtonStateWaitingForReleaseDebounceCompletion;
                if (self->callbackPressed != NULL)
                {
                    if ((!self->wasLongPressed) && (elapsedTime > self->pressedTimeMs))
                    {
                        self->callbackPressed();
                    }
                }
            }
            break;

        case hidButtonStateWaitingForReleaseDebounceCompletion:
            if (now > (self->time + self->debounceTimeMs))
            {
                self->state = hidButtonStateWaitingToBePressed;
            }
            break;

        default:
            break;
    }
}
