#ifndef DRIVERS_HID_BUTTON_H
#define DRIVERS_HID_BUTTON_H

/**
 ***********************************************************************************************************************
 * @file    button.h
 * @author  José Roberto Colombo Junior
 * @brief   Header file of the button driver.
 ***********************************************************************************************************************
 *
 * This library adds support to button behaviour through periodically sampled
 * strategy, i.e., the gpio pin where the button is attached is periodically
 * sampled. Due the nature of the button, the sample time does not need to
 * meet hard real-time constraints.
 *
 * @attention
 *
 * This is free software licensed under GPL.
 *
 ***********************************************************************************************************************
 */

#include "lib/architecture/gpio.h"
#include "lib/utils/time.h"

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Pressed example:
 *
 * -----------+  +--+  +--+                  +--+  +--+  +----+-----
 *            |  |  |  |  |                  |  |  |  |  |
 *            +--+  +--+  +----+-------------+  +--+  +--+
 * |<- idle ->|<- debouncing ->|<- pressed ->|<- debouncing ->|<- released
 *
 *
 *
 * Long pressed example:
 *
 * -----------+  +--+  +--+                                               +--+  +--+  +----+-----
 *            |  |  |  |  |                                               |  |  |  |  |
 *            +--+  +--+  +----+-------------+----------------------------+  +--+  +--+
 * |<- idle ->|<- debouncing ->|<- pressed ->|<-      long pressed      ->|<- debouncing ->|<- released
 *
 */

typedef enum hidButtonState {
    hidButtonStateWaitingToBePressed = 0,
    hidButtonStateWaitingForPressedDebounceCompletion,
    hidButtonStateWaitingForReleaseTransition,
    hidButtonStateWaitingForReleaseDebounceCompletion,
} hidButtonState;

typedef struct hidButton {
    bool logicStateWhenPressed;
    bool wasLongPressed;
    hidButtonState state;
    uint32_t time;
    uint32_t debounceTimeMs;
    uint32_t pressedTimeMs;
    uint32_t longPressedTimeMs;
    uint8_t (*gpioRead)();
    void (*callbackPressed)();
    void (*callbackLongPressed)();
    void (*callbackFirstPressed)();
} hidButton_t;

extern void hidButtonInit(hidButton_t *self, uint8_t (*gpioRead)());

extern void hidButtonSetPressedTime(hidButton_t *self, uint32_t msecs);

extern void hidButtonSetLongPressedTime(hidButton_t *self, uint32_t msecs);

extern void hidButtonAttachFirstPressed(hidButton_t* self, void (*onFirstPressed)());

extern void hidButtonSetDebounceTime(hidButton_t *self, uint32_t msecs);

extern void hidButtonAttachPressed(hidButton_t *self, void (*onPressed)());

extern void hidButtonAttachLongPressed(hidButton_t *self, void (*onLongPressed)());

extern void hidButtonRunPeriodically(hidButton_t *self);

#ifdef __cplusplus
}
#endif

#endif //DRIVERS_HID_BUTTON_H
