#include "hd44780.h"

#define hd44780WriteNow 1
#define hd44780WriteToBuffer 0

static void hd44780Write4bits(hd44780_t* self, uint8_t value)
{
    self->writeEN(1, hd44780WriteToBuffer);
    self->writeD7((value & 0x80) > 0, hd44780WriteToBuffer);
    self->writeD6((value & 0x40) > 0, hd44780WriteToBuffer);
    self->writeD5((value & 0x20) > 0, hd44780WriteToBuffer);
    self->writeD4((value & 0x10) > 0, hd44780WriteNow);
    delayUs(1);                // enable pulse must be >450ns
    self->writeEN(0, hd44780WriteNow);
    delayUs(50);               // commands need > 37us to settle
}

static void hd44780Send(hd44780_t* self, uint8_t value)
{
    hd44780Write4bits(self, value);
    hd44780Write4bits(self, value << 4);
}

static void hd44780Command(hd44780_t* self, uint8_t value)
{
    hd44780Send(self, value);
}

static size_t hd44780Write(hd44780_t* self, uint8_t value)
{
    self->writeRS(1, hd44780WriteToBuffer);
    hd44780Send(self, value);
    self->writeRS(0, hd44780WriteNow);
    return 1;
}

void hd44780Init(hd44780_t* self, uint8_t rows, void (*writeBL)(uint8_t, uint8_t), void (*writeRS)(uint8_t, uint8_t), void (*writeRW)(uint8_t, uint8_t), void (*writeEN)(uint8_t, uint8_t), void (*writeD4)(uint8_t, uint8_t), void (*writeD5)(uint8_t, uint8_t), void (*writeD6)(uint8_t, uint8_t), void (*writeD7)(uint8_t, uint8_t))
{
    self->rows = rows;
    self->writeBL = writeBL;
    self->writeRS = writeRS;
    self->writeRW = writeRW;
    self->writeEN = writeEN;
    self->writeD4 = writeD4;
    self->writeD5 = writeD5;
    self->writeD6 = writeD6;
    self->writeD7 = writeD7;
    self->displayfunction = LCD_HD44780_4BITMODE | LCD_HD44780_1LINE | LCD_HD44780_5x8DOTS;

    if (rows > 1)
    {
        self->displayfunction |= LCD_HD44780_2LINE;
    }

    // SEE PAGE 45/46 FOR INITIALIZATION SPECIFICATION!
    // according to datasheet, we need at least 40ms after power rises above 2.7V
    // before sending commands. Arduino can turn on way befer 4.5V so we'll wait 50
    delayMs(50);

    // Now we pull both RS and R/W low to begin commands
    self->writeRS(0, hd44780WriteToBuffer);
    self->writeRW(0, hd44780WriteToBuffer);
    self->writeEN(0, hd44780WriteNow);
    delayMs(1000);

    //put the LCD into 4 bit mode
    // this is according to the hitachi HD44780 datasheet
    // figure 24, pg 46

    // we start in 8bit mode, try to set 4 bit mode
    hd44780Write4bits(self, 0x03 << 4);
    delayUs(4500); // wait min 4.1ms

    // second try
    hd44780Write4bits(self, 0x03 << 4);
    delayUs(4500); // wait min 4.1ms

    // third go!
    hd44780Write4bits(self, 0x03 << 4);
    delayUs(150);

    // finally, set to 4-bit interface
    hd44780Write4bits(self, 0x02 << 4);

    // set # lines, font size, etc.
    hd44780Command(self, LCD_HD44780_FUNCTIONSET | self->displayfunction);

    // turn the display on with no cursor or blinking default
    self->displaycontrol = LCD_HD44780_DISPLAYON; // | LCD_CURSOROFF | LCD_BLINKOFF;
    hd44780Display(self);

    // clear it off
    hd44780Clear(self);

    // Initialize to default text direction (for roman languages)
    self->displaymode = LCD_HD44780_ENTRYLEFT | LCD_HD44780_ENTRYSHIFTDECREMENT;

    // set the entry mode
    hd44780Command(self, LCD_HD44780_ENTRYMODESET | self->displaymode);

    hd44780Home(self);
}

void hd44780Clear(hd44780_t* self)
{
    hd44780Command(self, LCD_HD44780_CLEARDISPLAY);       // clear display, set cursor position to zero
    delayMs(2);                                  // this command takes a long time!
}

void hd44780Home(hd44780_t* self)
{
    hd44780Command(self, LCD_HD44780_RETURNHOME);        // set cursor position to zero
    delayMs(2);                                  // this command takes a long time!
}

// Turn the display on/off (quickly)
void hd44780NoDisplay(hd44780_t* self)
{
    self->displaycontrol &= ~LCD_HD44780_DISPLAYON;
    hd44780Command(self, LCD_HD44780_DISPLAYCONTROL | self->displaycontrol);
}

void hd44780Display(hd44780_t* self)
{
    self->displaycontrol |= LCD_HD44780_DISPLAYON;
    hd44780Command(self, LCD_HD44780_DISPLAYCONTROL | self->displaycontrol);
}

// Turn on and off the blinking cursor
void hd44780NoBlink(hd44780_t* self)
{
    self->displaycontrol &= ~LCD_HD44780_BLINKON;
    hd44780Command(self, LCD_HD44780_DISPLAYCONTROL | self->displaycontrol);
}

void hd44780Blink(hd44780_t* self)
{
    self->displaycontrol |= LCD_HD44780_BLINKON;
    hd44780Command(self, LCD_HD44780_DISPLAYCONTROL | self->displaycontrol);
}

// Turns the underline cursor on/off
void hd44780NoCursor(hd44780_t* self)
{
    self->displaycontrol &= ~LCD_HD44780_CURSORON;
    hd44780Command(self, LCD_HD44780_DISPLAYCONTROL | self->displaycontrol);
}

void hd44780Cursor(hd44780_t* self)
{
    self->displaycontrol |= LCD_HD44780_CURSORON;
    hd44780Command(self, LCD_HD44780_DISPLAYCONTROL | self->displaycontrol);
}

// These commands scroll the display without changing the RAM
void hd44780ScrollDisplayLeft(hd44780_t* self)
{
    hd44780Command(self, LCD_HD44780_CURSORSHIFT | LCD_HD44780_DISPLAYMOVE | LCD_HD44780_MOVELEFT);
}

void hd44780ScrollDisplayRight(hd44780_t* self)
{
    hd44780Command(self, LCD_HD44780_CURSORSHIFT | LCD_HD44780_DISPLAYMOVE | LCD_HD44780_MOVERIGHT);
}

// This is for text that flows Left to Right
void hd44780LeftToRight(hd44780_t* self)
{
    self->displaymode |= LCD_HD44780_ENTRYLEFT;
    hd44780Command(self, LCD_HD44780_ENTRYMODESET | self->displaymode);
}

// This is for text that flows Right to Left
void hd44780RightToLeft(hd44780_t* self)
{
    self->displaymode &= ~LCD_HD44780_ENTRYLEFT;
    hd44780Command(self, LCD_HD44780_ENTRYMODESET | self->displaymode);
}

// Turn the (optional) backlight off/on
void hd44780NoBacklight(hd44780_t* self)
{
    self->writeBL(0, hd44780WriteNow);
}

void hd44780Backlight(hd44780_t* self)
{
    self->writeBL(1, hd44780WriteNow);
}

// This will 'right justify' text from the cursor
void hd44780Autoscroll(hd44780_t* self)
{
    self->displaymode |= LCD_HD44780_ENTRYSHIFTINCREMENT;
    hd44780Command(self, LCD_HD44780_ENTRYMODESET | self->displaymode);
}

// This will 'left justify' text from the cursor
void hd44780NoAutoscroll(hd44780_t* self)
{
    self->displaymode &= ~LCD_HD44780_ENTRYSHIFTINCREMENT;
    hd44780Command(self, LCD_HD44780_ENTRYMODESET | self->displaymode);
}

// Allows us to fill the first 8 CGRAM locations
// with custom characters
void hd44780CreateChar(hd44780_t* self, uint8_t location, uint8_t charmap[])
{
    uint8_t i;
    location &= 0x7; // we only have 8 locations 0-7
    hd44780Command(self, LCD_HD44780_SETCGRAMADDR | (location << 3));
    for (i = 0; i < 8; i++)
    {
        hd44780Write(self, charmap[i]);
    }
}

void hd44780SetCursor(hd44780_t* self, uint8_t col, uint8_t row)
{
    const uint8_t row_offsets[] = { 0x00, 0x40, 0x14, 0x54 };
    if (row > self->rows)
    {
        row = self->rows - 1;    // we count rows starting w/0
    }
    hd44780Command(self, LCD_HD44780_SETDDRAMADDR | (col + row_offsets[row]));
}

void hd44780Print(hd44780_t* self, char *str)
{
    unsigned char i = 0;
    // loop through 5 bytes
    while (str[i] != '\0')
    {
        //read characters and increment index
        hd44780Write(self, str[i++]);
    }
}
