#ifndef DRIVERS_DISPLAY_HD44780_H_
#define DRIVERS_DISPLAY_HD44780_H_

/**
 ***********************************************************************************************************************
 * @file    hd44780.h
 * @author  José Roberto Colombo Junior
 * @brief   Header file of the display HD44780 driver.
 ***********************************************************************************************************************
 *
 * This file implements basic support to handle the display HD44780. Instead of storing pin number and gpio handlers,
 * this driver stores pointers to gpio writing functions. This allows the user to reuse this driver even if the display
 * is connected to an I/O expander chip.
 *
 * @note This driver was based on https://github.com/fdebrabander/Arduino-LiquidCrystal-I2C-library
 * @attention This is free software licensed under GPL.
 *
 ***********************************************************************************************************************
 */

#include "lib/utils/time.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief The HD44780 model struct.
 *
 * It is assumed that each HD44780 display will have one instance of this struct.
 */
typedef struct hd44780 {
    uint8_t displayfunction;
    uint8_t displaycontrol;
    uint8_t displaymode;
    uint8_t rows;
    void (*writeBL)(uint8_t level, uint8_t commit);   ///< A pointer to a function to write on backlight pin.
    void (*writeRS)(uint8_t level, uint8_t commit);   ///< A pointer to a function to write on RS pin.
    void (*writeRW)(uint8_t level, uint8_t commit);   ///< A pointer to a function to write on RW pin.
    void (*writeEN)(uint8_t level, uint8_t commit);   ///< A pointer to a function to write on EN pin.
    void (*writeD4)(uint8_t level, uint8_t commit);   ///< A pointer to a function to write on D4 pin.
    void (*writeD5)(uint8_t level, uint8_t commit);   ///< A pointer to a function to write on D5 pin.
    void (*writeD6)(uint8_t level, uint8_t commit);   ///< A pointer to a function to write on D6 pin.
    void (*writeD7)(uint8_t level, uint8_t commit);   ///< A pointer to a function to write on D7 pin.
} hd44780_t;

/**
 * @brief Set the LCD display in the correct begin state, must be called before
 * anything else is done.
 *
 * @param rows is the display's number of rows
 * @param writeBL
 * @param writeRS
 * @param writeRW
 * @param writeEN
 * @param writeD4
 * @param writeD5
 * @param writeD6
 * @param writeD7
 */
void
hd44780Init(hd44780_t *self, uint8_t rows, void (*writeBL)(uint8_t level, uint8_t commit),
            void (*writeRS)(uint8_t level, uint8_t commit), void (*writeRW)(uint8_t level, uint8_t commit),
            void (*writeEN)(uint8_t level, uint8_t commit), void (*writeD4)(uint8_t level, uint8_t commit),
            void (*writeD5)(uint8_t level, uint8_t commit), void (*writeD6)(uint8_t level, uint8_t commit),
            void (*writeD7)(uint8_t level, uint8_t commit));

/**
 * Remove all the characters currently shown. Next print/write operation will start
 * from the first position on LCD display.
 */
void hd44780Clear(hd44780_t *self);

/**
 * Next print/write operation will will start from the first position on the LCD display.
 */
void hd44780Home(hd44780_t *self);

/**
 * Do not show any characters on the LCD display. Backlight state will remain unchanged.
 * Also all characters written on the display will return, when the display in enabled again.
 */
void hd44780NoDisplay(hd44780_t *self);

/**
 * Show the characters on the LCD display, this is the normal behaviour. This method should
 * only be used after noDisplay() has been used.
 */
void hd44780Display(hd44780_t *self);

/**
 * Do not blink the cursor indicator.
 */
void hd44780NoBlink(hd44780_t *self);

/**
 * Start blinking the cursor indicator.
 */
void hd44780Blink(hd44780_t *self);

/**
 * Do not show a cursor indicator.
 */
void hd44780NoCursor(hd44780_t *self);

/**
 * Show a cursor indicator, cursor can blink on not blink. Use the
 * methods blink() and noBlink() for changing cursor blink.
 */
void hd44780Cursor(hd44780_t *self);

void hd44780ScrollDisplayLeft(hd44780_t *self);

void hd44780ScrollDisplayRight(hd44780_t *self);

void hd44780LeftToRight(hd44780_t *self);

void hd44780RightToLeft(hd44780_t *self);

void hd44780NoBacklight(hd44780_t *self);

void hd44780Backlight(hd44780_t *self);

bool hd44780GetBacklight(hd44780_t *self);

void hd44780Autoscroll(hd44780_t *self);

void hd44780NoAutoscroll(hd44780_t *self);

void hd44780CreateChar(hd44780_t *self, uint8_t, uint8_t[]);

void hd44780SetCursor(hd44780_t *self, uint8_t, uint8_t);

void hd44780Print(hd44780_t *self, char *str);

// HD44780 defined commands
#define LCD_HD44780_CLEARDISPLAY        0x01
#define LCD_HD44780_RETURNHOME          0x02
#define LCD_HD44780_ENTRYMODESET        0x04
#define LCD_HD44780_DISPLAYCONTROL      0x08
#define LCD_HD44780_CURSORSHIFT         0x10
#define LCD_HD44780_FUNCTIONSET         0x20
#define LCD_HD44780_SETCGRAMADDR        0x40
#define LCD_HD44780_SETDDRAMADDR        0x80

// flags for display entry mode
#define LCD_HD44780_ENTRYRIGHT          0x00
#define LCD_HD44780_ENTRYLEFT           0x02
#define LCD_HD44780_ENTRYSHIFTINCREMENT 0x01
#define LCD_HD44780_ENTRYSHIFTDECREMENT 0x00

// flags for display on/off control
#define LCD_HD44780_DISPLAYON           0x04
#define LCD_HD44780_DISPLAYOFF          0x00
#define LCD_HD44780_CURSORON            0x02
#define LCD_HD44780_CURSOROFF           0x00
#define LCD_HD44780_BLINKON             0x01
#define LCD_HD44780_BLINKOFF            0x00

// flags for display/cursor shift
#define LCD_HD44780_DISPLAYMOVE         0x08
#define LCD_HD44780_CURSORMOVE          0x00
#define LCD_HD44780_MOVERIGHT           0x04
#define LCD_HD44780_MOVELEFT            0x00

// flags for function set
#define LCD_HD44780_8BITMODE            0x10
#define LCD_HD44780_4BITMODE            0x00
#define LCD_HD44780_2LINE               0x08
#define LCD_HD44780_1LINE               0x00
#define LCD_HD44780_5x10DOTS            0x04
#define LCD_HD44780_5x8DOTS             0x00

#ifdef __cplusplus
}
#endif

#endif /* DRIVERS_DISPLAY_HD44780_H_ */
