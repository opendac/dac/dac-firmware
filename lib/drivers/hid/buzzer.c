#include "buzzer.h"


void buzzerMute(TIM_HandleTypeDef *htim, uint32_t channel)
{
    timerSetDuty(htim, channel, 0);
}

void buzzeBeep(TIM_HandleTypeDef *htim, uint32_t channel, float frequencyHz)
{
    timerSetFrequency(htim, frequencyHz);
    timerSetDuty(htim, channel, 0.5);
}
