#ifndef DRIVERS_HAL_ONEWIRE_H_
#define DRIVERS_HAL_ONEWIRE_H_


// This library was based on
// https://stm32withoutfear.blogspot.com/2019/05/stm32-onewire-dallas-temperature-hal.html
// https://github.com/taburyak/STM32_OneWire_DallasTemperature_HAL_UART_DMA

#ifndef pgm_read_byte
#define pgm_read_byte(addr) (*(const uint8_t *)(addr))
#endif

#include "lib/utils/time.h"
#if defined(STM32F1)
#include "stm32f1xx_hal_uart.h"
#elif defined(STM32F3)
#include "stm32f3xx_hal_uart.h"
#elif defined(STM32F4)
#include "stm32f4xx_hal_uart.h"
#elif defined(STM32F7)
#include "stm32f7xx_hal_uart.h"
#endif

namespace hal::onewire {
    constexpr uint8_t OW_SEND_RESET = 1;
    constexpr uint8_t OW_NO_RESET   = 1;
    constexpr uint8_t OW_OK         = 1;
    constexpr uint8_t OW_ERROR      = 2;
    constexpr uint8_t OW_NO_DEVICE  = 3;
    constexpr uint8_t OW_NO_READ    = 0xFF;
    constexpr uint8_t OW_READ_SLOT  = 0xFF;
    constexpr uint8_t OW_PRESENCE   = 0xF0;
    constexpr uint8_t bitHigh       = 0xFF;
    constexpr uint8_t bitLow        = 0x00;

    class Bus {
    public:
        Bus() = default;
        ~Bus() = default;
        void begin(GPIO_TypeDef* setPort, uint32_t setPinPosition, UART_HandleTypeDef *setHuart);

        // Perform a 1-Wire reset cycle. Returns 1 if a device responds
        // with a presence pulse.  Returns 0 if there is no device or the
        // bus is shorted or otherwise held low for more than 250uS
        //
        // Returns 1 if a device asserted a presence pulse, 0 otherwise.
        uint8_t reset();

        // Issue a 1-Wire rom select command, you do the reset first.
        void select(const uint8_t rom[8]);

        // Issue a 1-Wire rom skip command, to address all on bus.
        void skip();

        void write_bit(uint8_t bit);
        void write(uint8_t v, uint8_t power = 0);
        void write_bytes(const uint8_t *buf, uint16_t count, bool power = 0);

        uint8_t read();
        uint8_t read_bytes(uint8_t *buf, uint16_t count);
        uint8_t read_bit();

        void depower();
        uint8_t crc8(const uint8_t *addr, uint8_t len);
        bool check_crc16(const uint8_t* input, uint16_t len, const uint8_t* inverted_crc, uint16_t crc = 0);
        uint16_t crc16(const uint8_t* input, uint16_t len, uint16_t crc = 0);

    protected:
        // Dow-CRC using polynomial X^8 + X^5 + X^4 + X^0
        // Tiny 2x16 entry CRC table created by Arjen Lentz
        // See http://lentz.com.au/blog/calculating-crc-with-a-tiny-32-entry-lookup-table
        static constexpr uint8_t dscrc2x16_table[] = {
                0x00, 0x5E, 0xBC, 0xE2, 0x61, 0x3F, 0xDD, 0x83,
                0xC2, 0x9C, 0x7E, 0x20, 0xA3, 0xFD, 0x1F, 0x41,
                0x00, 0x9D, 0x23, 0xBE, 0x46, 0xDB, 0x65, 0xF8,
                0x8C, 0x11, 0xAF, 0x32, 0xCA, 0x57, 0xE9, 0x74
        };

    private:
        UART_HandleTypeDef* huart;
        GPIO_TypeDef* port;
        uint32_t pin_position;
    };


}

#endif /* DRIVERS_HAL_ONEWIRE_H_ */
