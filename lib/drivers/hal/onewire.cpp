#include "onewire.h"
#include "lib/architecture/gpio.h"
#include "lib/utils/compatibility/arduino.h"

using namespace hal::onewire;

void Bus::begin(GPIO_TypeDef* setPort, uint32_t setPinPosition, UART_HandleTypeDef *setHuart)
{
    // auxiliary variables
    GPIO_InitTypeDef config;

    // copy information
    port = setPort;
    pin_position = setPinPosition;
    huart = setHuart;

    // configure the gpio pin
    config.Pin = setPinPosition;
    config.Pull = GPIO_PULLUP;
    config.Speed = GPIO_SPEED_FREQ_HIGH;
    config.Mode = GPIO_MODE_AF_OD;
    HAL_GPIO_Init(port, &config);

    // configure the uart
    huart->Init.BaudRate = 9600;
    huart->Init.WordLength = UART_WORDLENGTH_8B;
    huart->Init.StopBits = UART_STOPBITS_1;
    huart->Init.Parity = UART_PARITY_NONE;
    huart->Init.Mode = UART_MODE_TX_RX;
    huart->Init.HwFlowCtl = UART_HWCONTROL_NONE;
    huart->Init.OverSampling = UART_OVERSAMPLING_16;
    HAL_HalfDuplex_Init(huart);
    depower();
}

uint8_t Bus::reset()
{
    uint8_t r;
    uint8_t retries = 125;

    depower();

    // wait until the wire is high... just in case
    do {
        if (--retries == 0) return 0;
        delayUs(2);
    } while ( (HAL_GPIO_ReadPin(port, pin_position) > 0));


    HAL_GPIO_WritePin(port, pin_position, (GPIO_PinState) 0);
    delayUs(480);
    noInterrupts();

    HAL_GPIO_WritePin(port, pin_position, (GPIO_PinState) 1); // allow it to float

    delayUs(70);
    r = (HAL_GPIO_ReadPin(port, pin_position) > 0);
    interrupts();
    delayUs(410);
    return r;
}

void Bus::select(const uint8_t rom[8])
{
    uint8_t i;
    write(0x55);           // Choose ROM
    for (i = 0; i < 8; i++) write(rom[i]);
}

void Bus::skip()
{
    write(0xCC);           // Skip ROM
}

void Bus::write_bit(uint8_t bit)
{
    uint8_t data = (bit > 0) ? bitHigh : bitLow;
    HAL_UART_Transmit(huart, &data, 1, HAL_MAX_DELAY);
}

// Compute a Dallas Semiconductor 8 bit CRC. These show up in the ROM
// and the registers.  (Use tiny 2x16 entry CRC table)
uint8_t Bus::crc8(const uint8_t *addr, uint8_t len)
{
    uint8_t crc = 0;

    while (len--) {
        crc = *addr++ ^ crc;  // just re-using crc as intermediate
        crc = pgm_read_byte(dscrc2x16_table + (crc & 0x0f)) ^
              pgm_read_byte(dscrc2x16_table + 16 + ((crc >> 4) & 0x0f));
    }

    return crc;
}

// Compute the 1-Wire CRC16 and compare it against the received CRC.
// Example usage (reading a DS2408):
//    // Put everything in a buffer so we can compute the CRC easily.
//    uint8_t buf[13];
//    buf[0] = 0xF0;    // Read PIO Registers
//    buf[1] = 0x88;    // LSB address
//    buf[2] = 0x00;    // MSB address
//    WriteBytes(net, buf, 3);    // Write 3 cmd bytes
//    ReadBytes(net, buf+3, 10);  // Read 6 data bytes, 2 0xFF, 2 CRC16
//    if (!CheckCRC16(buf, 11, &buf[11])) {
//        // Handle error.
//    }
//
// @param input - Array of bytes to checksum.
// @param len - How many bytes to use.
// @param inverted_crc - The two CRC16 bytes in the received data.
//                       This should just point into the received data,
//                       *not* at a 16-bit integer.
// @param crc - The crc starting value (optional)
// @return True, iff the CRC matches.
bool Bus::check_crc16(const uint8_t* input, uint16_t len, const uint8_t* inverted_crc, uint16_t crc)
{
    crc = ~crc16(input, len, crc);
    return (crc & 0xFF) == inverted_crc[0] && (crc >> 8) == inverted_crc[1];
}

// Compute a Dallas Semiconductor 16 bit CRC.  This is required to check
// the integrity of data received from many 1-Wire devices.  Note that the
// CRC computed here is *not* what you'll get from the 1-Wire network,
// for two reasons:
//   1) The CRC is transmitted bitwise inverted.
//   2) Depending on the endian-ness of your processor, the binary
//      representation of the two-byte return value may have a different
//      byte order than the two bytes you get from 1-Wire.
// @param input - Array of bytes to checksum.
// @param len - How many bytes to use.
// @param crc - The crc starting value (optional)
// @return The CRC16, as defined by Dallas Semiconductor.
uint16_t Bus::crc16(const uint8_t* input, uint16_t len, uint16_t crc)
{
    static const uint8_t oddparity[16] =
            { 0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0 };

    for (uint16_t i = 0 ; i < len ; i++) {
        // Even though we're just copying a byte from the input,
        // we'll be doing 16-bit computation with it.
        uint16_t cdata = input[i];
        cdata = (cdata ^ crc) & 0xff;
        crc >>= 8;

        if (oddparity[cdata & 0x0F] ^ oddparity[cdata >> 4])
            crc ^= 0xC001;

        cdata <<= 6;
        crc ^= cdata;
        cdata <<= 1;
        crc ^= cdata;
    }
    return crc;
}


//u8 NewOneWire::reset(void)
//{
//    uint8_t ow_presence = OW_PRESENCE;
//
//    set_baud(9600);
//
//    HAL_UART_Transmit(huart, &ow_presence, 1, HAL_MAX_DELAY);
//    HAL_UART_Receive_DMA(huart, &ow_presence, 1);
//
//    // Wait for the end of the transfer
//    while (HAL_UART_GetState(huart) != HAL_UART_STATE_READY)
//    {
//        __NOP();
//    }
//
//    set_baud(115200);
//
//    if (ow_presence != OW_PRESENCE)
//    {
//        return OW_OK;
//    }
//
//    return OW_NO_DEVICE;
//}
//
//
//void NewOneWire::write_bit(uint8_t v)
//{
//    if (v > 0)
//        HAL_UART_Transmit(huart, &bit_high, 1, HAL_MAX_DELAY);
//    else
//        HAL_UART_Transmit(huart, &bit_low, 1, HAL_MAX_DELAY);
//}
//
//
//uint8_t NewOneWire::read_bit(void)
//{
//    u8 value, buffer;
//
//    HAL_UART_Transmit(huart, &bit_high, 1, HAL_MAX_DELAY);
//    HAL_UART_Receive_DMA(huart, &value, 1);
//    while (HAL_UART_GetState(huart) != HAL_UART_STATE_READY)
//    {
//        __NOP();
//    }
//
//    value = (buffer == bit_high);
//    return value;
//}
