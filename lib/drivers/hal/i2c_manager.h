#ifndef DRIVERS_HAL_I2C_H
#define DRIVERS_HAL_I2C_H

#include "lib/architecture/i2c.h"
#include "lib/utils/fifo.h"

#define i2cManagerUseDma
#define i2cJobMaxNumber 32

#ifdef __cplusplus
extern "C" {
#endif

typedef enum i2cCmd {
    i2cCmdRead = 0,
    i2cCmdWrite,
    i2cCmdMemRead,
    i2cCmdMemWrite,
} i2cCmd_t;

typedef enum i2cManagerState {
    i2cManagerStateUninitialized = 0,
    i2cManagerStateReady,
    i2cManagerStateBusy,
} i2cManagerState_t;

typedef struct i2cJob {
    i2cCmd_t cmd;
    uint16_t size;
    uint8_t deviceAddress;
    uint8_t deviceMemoryAddress;
    void *data;
    void (*callbackFinished)(void *data, uint16_t size);
} i2cJob_t;

typedef struct i2cManager {
    I2C_HandleTypeDef *hi2c;
    fifo queue;
    uint8_t storage[i2cJobMaxNumber * sizeof(i2cJob_t)];
    i2cManagerState_t state;
} i2cManager_t;

extern void i2cManagerInit(i2cManager_t *manager, I2C_HandleTypeDef *hi2c);

extern uint32_t i2cManagerWork(i2cManager_t *manager);

extern bool
i2cManagerAddJob(i2cManager_t *manager, i2cCmd_t cmd, uint8_t deviceAddress, uint8_t deviceMemoryAddress, uint8_t *data,
                 uint16_t size, void (*callbackFinished)(void *data, uint16_t size));

#define i2cManagerAddReadJob(manager, deviceAddress, data, size, callback)                           i2cManagerAddJob(manager, i2cCmdRead, deviceAddress, 0, data, size, callback)
#define i2cManagerAddWriteJob(manager, deviceAddress, data, size, callback)                          i2cManagerAddJob(manager, i2cCmdWrite, deviceAddress, 0, data, size, callback)
#define i2cManagerAddMemReadJob(manager, deviceAddress, data, deviceMemoryAddress, size, callback)   i2cManagerAddJob(manager, i2cCmdMemRead, deviceAddress, deviceMemoryAddress, data, size, callback)
#define i2cManagerAddMemWriteJob(manager, deviceAddress, data, deviceMemoryAddress, size, callback)  i2cManagerAddJob(manager, i2cCmdMemWrite, deviceAddress, deviceMemoryAddress, data, size, callback)

extern void i2cManagerCallbackFinished(i2cManager_t *manager);

#ifdef __cplusplus
}
#endif

#endif //DRIVERS_HAL_I2C_H
