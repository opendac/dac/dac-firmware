#ifndef DRIVERS_HAL_GPIO_GPIO_H_
#define DRIVERS_HAL_GPIO_GPIO_H_

/**
 ******************************************************************************
 * @file    gpio.h
 * @author  José Roberto Colombo Junior
 * @brief   Header file of the gpio driver.
 ******************************************************************************
 *
 * This library adds another abstraction layer relating digital gpio pins. The
 * idea here is to provide easy to use objects to control local or remote pins.
 * By remote pins I mean I/O expanders.
 * If your architecture is not supported, you may use some low level driver provided by
 * the manufacturer or write your own low level functions.
 *
 * @attention
 *
 * This is free software licensed under GPL.
 *
 ******************************************************************************
 */

#include "lib/architecture/gpio.h"

namespace gpio {

    class BaseDigitalPin {
    public:
        explicit BaseDigitalPin(uint32_t setPinPosition) : pinPosition(setPinPosition) {};
        ~BaseDigitalPin() = default;

        /**
         * @brief Changes the state of a digital pin. The output voltage depends on the architecture.
         * @param value is the new level of the output pin.
         */
        virtual void write(uint8_t state) = 0;

        /**
         * @brief Changes the state of a digital pin. The output voltage depends on the architecture. The I/O
         *        expanders are usually connected to the microcontroller using communication lines. When it is
         *        required to change the state of many remote pins, many communication requests may be done. This
         *        parameter allows the user to make all the pin changes and only after changing all the pin states,
         *        communicate.
         * @param value is the new level of the output pin.
         */
        virtual void writeLater(uint8_t state) = 0;

        /**
        * @brief Reads the value from a digital pin.
        * @return The logic state of the pin: false or positive number.
        */
        virtual uint8_t read() = 0;

        virtual uint8_t readFromBuffer() = 0;

    protected:
        const uint32_t pinPosition;
    };

    class DigitalPin : public BaseDigitalPin {
    public:
        explicit DigitalPin(gpioPort* setPort, uint32_t setPinPosition) : BaseDigitalPin(setPinPosition), port(setPort) {}
        ~DigitalPin() = default;

        void init(gpioMode mode)
        {
            gpioSetMode(port, pinPosition, mode);
        }

        void write(uint8_t state) override
        {
            gpioWrite(port, pinPosition, state);
        }

        void writeLater(uint8_t state) override
        {
            gpioWrite(port, pinPosition, state);
        }

        uint8_t read() override
        {
            return gpioRead(port, pinPosition);
        }

        uint8_t readFromBuffer() override
        {
            return gpioRead(port, pinPosition);
        }

    protected:
        gpioPort* const port;
    };

}  // namespace gpio

#endif /* DRIVERS_HAL_GPIO_GPIO_H_ */
