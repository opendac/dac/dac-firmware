#ifndef DRIVERS_HAL_TIMER_H
#define DRIVERS_HAL_TIMER_H


/**
 ******************************************************************************
 * @file    timer.h
 * @author  José Roberto Colombo Junior
 * @brief   Header file of the timer driver.
 ******************************************************************************
 *
 * This library adds another abstraction layer to the timers peripheral.
 *
 * If your architecture is not supported, you may use some low level driver
 * provided by the manufacturer or write your own low level functions.
 *
 * @attention
 *
 * This is free software licensed under GPL.
 *
 ******************************************************************************
  */


#include "lib/utils/types.h"
#include "lib/architecture/timer.h"

void timerSetFrequency(TIM_HandleTypeDef* htim, float newFreqHz);

void timerSetDuty(TIM_HandleTypeDef* htim, uint32_t channel, float newDuty);


#endif // DRIVERS_HAL_TIMER_H
