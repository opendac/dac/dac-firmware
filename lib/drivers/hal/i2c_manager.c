#include "lib/drivers/hal/i2c_manager.h"

void i2cManagerInit(i2cManager_t *self, I2C_HandleTypeDef *hi2c)
{
    self->hi2c = hi2c;
    fifoInit(&self->queue, i2cJobMaxNumber, sizeof(i2cJob_t), self->storage);
    self->state = i2cManagerStateReady;
}

bool i2cManagerAddJob(i2cManager_t *manager, i2cCmd_t cmd, uint8_t deviceAddress, uint8_t deviceMemoryAddress, uint8_t* data, uint16_t size, void (*callbackFinished)(void* data, uint16_t size))
{
    bool wasAdded = false;
    i2cJob_t* job = (i2cJob_t*) fifoReserve(&manager->queue);
    if (job != NULL)
    {
        job->cmd = cmd;
        job->deviceAddress = deviceAddress;
        job->deviceMemoryAddress = deviceMemoryAddress;
        job->data = data;
        job->size = size;
        job->callbackFinished = callbackFinished;
        wasAdded = true;
    }

    // Start to work
    i2cManagerWork(manager);

    return wasAdded;
}

uint32_t i2cManagerWork(i2cManager_t *manager)
{
    HAL_StatusTypeDef status;
    uint32_t estimatedTransactionTimeUs = 0;
    i2cJob_t* job;

    if ((manager->state == i2cManagerStateReady) && (manager->hi2c->State == HAL_I2C_STATE_READY))
    {
        if (fifoIsNotEmpty(&manager->queue))
        {
            manager->state = i2cManagerStateBusy;

            // Get a job
            job = fifoPeekOldest(&manager->queue);

            switch (job->cmd) {
#if defined(i2cManagerUseDma)
                case i2cCmdRead:
                    status = HAL_I2C_Master_Receive_DMA(manager->hi2c, job->deviceAddress, job->data, job->size);
                    break;
                case i2cCmdWrite:
                    status = HAL_I2C_Master_Transmit_DMA(manager->hi2c, job->deviceAddress, job->data, job->size);
                    break;
                case i2cCmdMemRead:
                    status = HAL_I2C_Mem_Read_DMA(manager->hi2c, job->deviceAddress, job->deviceMemoryAddress, I2C_MEMADD_SIZE_8BIT, job->data, job->size);
                    break;
                case i2cCmdMemWrite:
                    status = HAL_I2C_Mem_Write_DMA(manager->hi2c, job->deviceAddress, job->deviceMemoryAddress, I2C_MEMADD_SIZE_8BIT, job->data, job->size);
                    break;
#else
                case i2cCmdRead:
                    status = HAL_I2C_Master_Receive_IT(manager->hi2c, job->deviceAddress, job->data, job->size);
                    break;
                case i2cCmdWrite:
                    status = HAL_I2C_Master_Transmit_IT(manager->hi2c, job->deviceAddress, job->data, job->size);
                    break;
                case i2cCmdMemRead:
                    status = HAL_I2C_Mem_Read_IT(manager->hi2c, job->deviceAddress, job->deviceMemoryAddress, I2C_MEMADD_SIZE_8BIT, job->data, job->size);
                    break;
                case i2cCmdMemWrite:
                    status = HAL_I2C_Mem_Write_IT(manager->hi2c, job->deviceAddress, job->deviceMemoryAddress, I2C_MEMADD_SIZE_8BIT, job->data, job->size);
                    break;
#endif
            }

            // estimate the transaction time
            estimatedTransactionTimeUs = (1000000 * (10 + 8 * job->size)) / manager->hi2c->Init.ClockSpeed;
        }
    }

    UNUSED(status);
    return estimatedTransactionTimeUs;
}

void i2cManagerCallbackFinished(i2cManager_t *manager)
{
    i2cJob_t job;

    if (manager->state == i2cManagerStateBusy)
    {
        // copy the finished job manifest and remove it from the job list
        fifoPull(&manager->queue, &job);

        // check if there are more i2c jobs to run
        manager->state = i2cManagerStateReady;
        i2cManagerWork(manager);

        // now that the i2c is communicating, run the old job callback
        if (job.callbackFinished != NULL)
        {
            job.callbackFinished(job.data, job.size);
        }
    }
}

