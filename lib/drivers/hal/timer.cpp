#include "lib/drivers/hal/timer.h"

// todo use the roundf function
void timerSetFrequency(TIM_HandleTypeDef* htim, float newFreqHz)
{
    /*
     * The frequency is calculated with:
     * ftimer = SystemCoreClock / ((actual_top + 1) * (actual_divider + 1))
     */
    const float max_top = 0xFFFF, max_divider = 0xFFFF;
    const auto ftimer = static_cast<float>(SystemCoreClock);
    const float fmin = ftimer / ((max_top + 1) * (max_divider + 1));
    int32_t top, divider;

    if (newFreqHz > ftimer)
    {
        divider = 0;
        top = 100;
    }
    else if (newFreqHz <= fmin)
    {
        divider = static_cast<int32_t>(max_divider);
        top = static_cast<int32_t>(max_top);
    }
    else
    {
        // a modest first guess
        divider = static_cast<int32_t>(ftimer / ((max_top + 1) * newFreqHz) - 1);
        top     = static_cast<int32_t>(ftimer / (static_cast<float>(divider + 1) * newFreqHz) - 1);

        while (top > static_cast<int32_t>(max_top))
        {
            divider = divider + 1;
            top     = static_cast<int32_t>(ftimer / (static_cast<float>(divider + 1) * newFreqHz) - 1);
        }
    }

    htim->Instance->ARR = (uint32_t) top;
    htim->Instance->PSC = (uint32_t) divider;
}

void timerSetDuty(TIM_HandleTypeDef* htim, uint32_t channel, float newDuty)
{
    /*
     * todo
     * Detect automatically the timer count register.
     */

    const float max_count = (float) (htim->Instance->ARR);
    const uint16_t raw_duty = (uint16_t) (max_count * newDuty);

    switch (channel) {
        case TIM_CHANNEL_1:
            htim->Instance->CCR1 = raw_duty;
            break;
        case TIM_CHANNEL_2:
            htim->Instance->CCR2 = raw_duty;
            break;
        case TIM_CHANNEL_3:
            htim->Instance->CCR3 = raw_duty;
            break;
        case TIM_CHANNEL_4:
            htim->Instance->CCR4 = raw_duty;
            break;
        default:
            break;
    }
}
