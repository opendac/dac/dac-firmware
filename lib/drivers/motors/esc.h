#ifndef DRIVERS_MOTORS_ESC_H
#define DRIVERS_MOTORS_ESC_H

#include "lib/utils/types.h"

/**
 * Implements a six-step table. See the AN857 of Microchip: "Brushless DC Motor Control Made Easy".
 *
 * @param hall is the state of the hall c, b, a sensor ordered as LSB (example 0b101).
 * @param a is the state of the phase a. It can be +1 (connected to Vcc), 0 (floating) or -1 (connected to -Vee).
 * @param b is the state of the phase b. It can be +1 (connected to Vcc), 0 (floating) or -1 (connected to -Vee).
 * @param c is the state of the phase c. It can be +1 (connected to Vcc), 0 (floating) or -1 (connected to -Vee).
 */
inline void escTable(uint8_t hall, int8_t* a, int8_t* b, int8_t* c)
{
    switch (hall) {
        case 0b001:
            *a = +1;
            *b = 0;
            *c = -1;
            break;

        case 0b011:
            *a = 0;
            *b = +1;
            *c = -1;
            break;

        case 0b010:
            *a = -1;
            *b = +1;
            *c = 0;
            break;

        case 0b110:
            *a = -1;
            *b = 0;
            *c = +1;
            break;

        case 0b100:
            *a = 0;
            *b = -1;
            *c = +1;
            break;

        case 0b101:
            *a = +1;
            *b = -1;
            *c = 0;
            break;

        default:
            break;
    }
}

#endif //DRIVERS_MOTORS_ESC_H
