#include "mcp4725.h"

#if defined(MCP4725_USE_I2C_MANAGER)
void mcp4725Init(mcp4725_t *self, mcp4725Address_t deviceAddress, i2cManager_t *i2cManager)
#else
void mcp4725Init(mcp4725_t *self, mcp4725Address_t deviceAddress, I2C_HandleTypeDef *hi2c)
#endif
{
    const uint8_t addresses[] = {
            mcp4725Address0,
            mcp4725Address1,
            mcp4725Address2,
            mcp4725Address3,
            mcp4725Address4,
            mcp4725Address5,
            mcp4725Address6,
            mcp4725Address7
    };
    HAL_StatusTypeDef status = HAL_ERROR;
    uint8_t i;

    // copy the i2c handler
#if defined(MCP4725_USE_I2C_MANAGER)
    self->i2cManager = i2cManager;
#else
    self->hi2c = hi2c;
#endif

    // should try to identify the mcp4725 address automatically?
    if (deviceAddress == mcp4725AddressAuto)
    {
        i = 0;
        while (status != HAL_OK && i < 8)
        {
            status = HAL_I2C_IsDeviceReady(self->i2cManager->hi2c, addresses[i], 2, 1);

            if (status == HAL_OK)
            {
                self->deviceAddress = addresses[i];
            }

            i = i + 1;
        }
    }
    else
    {
        self->deviceAddress = deviceAddress;
    }
}

void mcp4725SetVoltage(mcp4725_t *self, uint16_t voltage)
{
#if defined(MCP4725_USE_I2C_MANAGER)
    self->buffer[0] = 0b01000000;   // write to DAC register
    self->buffer[1] = voltage / 16;
    self->buffer[2] = (voltage % 16) << 4;
    i2cManagerAddWriteJob(self->i2cManager, self->deviceAddress, self->buffer, 3, NULL);
#else
    uint8_t buffer[] = {
            0b01000000,        // write to DAC register
            voltage / 16,
            (voltage % 16) << 4
    };
    HAL_StatusTypeDef status = HAL_I2C_Master_Transmit(self->hi2c, self->deviceAddress, buffer, sizeof(buffer), 1);
#endif
}
