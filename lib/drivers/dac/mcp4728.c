#include "mcp4728.h"

#if defined(MCP4728_USE_I2C_MANAGER)
void mcp4728Init(mcp4728_t *self, mcp4728Address_t deviceAddress, i2cManager_t *i2cManager)
{
    self->deviceAddress = deviceAddress;
    self->i2cManager = i2cManager;
}
#else
void mcp4728Init(mcp4728_t *self, mcp4728Address_t deviceAddress, I2C_HandleTypeDef *hi2c)
{
    self->deviceAddress = deviceAddress;
    self->hi2c = hi2c;
}
#endif

void mcp4728SetVoltages(mcp4728_t *self, uint16_t A, uint16_t B, uint16_t C, uint16_t D)
{
#if defined(MCP4728_USE_I2C_MANAGER)
    uint8_t *buffer = self->buffer;
#else
    uint8_t buffer[8];
#endif

    // copy data into buffer
    buffer[0] = A >> 8;
    buffer[1] = A & 0xFF;
    buffer[2] = B >> 8;
    buffer[3] = B & 0xFF;
    buffer[4] = C >> 8;
    buffer[5] = C & 0xFF;
    buffer[6] = D >> 8;
    buffer[7] = D & 0xFF;

#if defined(MCP4728_USE_I2C_MANAGER)
    i2cManagerAddWriteJob(self->i2cManager, self->deviceAddress, self->buffer, 8, NULL);
#else
    HAL_I2C_Master_Transmit(self->hi2c, self->deviceAddress, buffer, sizeof(buffer), 1);
#endif
}

