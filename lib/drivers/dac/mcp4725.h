#ifndef DRIVERS_DAC_MCP4725_H
#define DRIVERS_DAC_MCP4725_H

#define MCP4725_USE_I2C_MANAGER

#ifdef MCP4725_USE_I2C_MANAGER
#include "lib/drivers/hal/i2c_manager.h"
#else
#include "lib/architecture/i2c.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

typedef enum mcp4725Address {
    mcp4725Address0 = 0b1100000 << 1,
    mcp4725Address1 = 0b1100001 << 1,
    mcp4725Address2 = 0b1100010 << 1,
    mcp4725Address3 = 0b1100011 << 1,
    mcp4725Address4 = 0b1100100 << 1,
    mcp4725Address5 = 0b1100101 << 1,
    mcp4725Address6 = 0b1100110 << 1,
    mcp4725Address7 = 0b1100111 << 1,
    mcp4725AddressAuto,
} mcp4725Address_t;

typedef struct mcp4725 {
    mcp4725Address_t deviceAddress;
#if defined(MCP4725_USE_I2C_MANAGER)
    i2cManager_t *i2cManager;
    uint8_t buffer[3];
#else
    I2C_HandleTypeDef* hi2c;
#endif
} mcp4725_t;

#if defined(MCP4725_USE_I2C_MANAGER)
extern void mcp4725Init(mcp4725_t *self, mcp4725Address_t deviceAddress, i2cManager_t *i2cManager);
#else
extern void mcp4725Init(mcp4725_t *self, mcp4725Address_t deviceAddress, I2C_HandleTypeDef *hi2c);
#endif

extern void mcp4725SetVoltage(mcp4725_t *self, uint16_t voltage);

#ifdef __cplusplus
}
#endif

#endif //DRIVERS_DAC_MCP4725_H
