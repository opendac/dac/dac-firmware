#ifndef DRIVERS_DAC_MCP4728_H
#define DRIVERS_DAC_MCP4728_H

//#define MCP4728_USE_I2C_MANAGER

#ifdef MCP4728_USE_I2C_MANAGER
#include "lib/drivers/hal/i2c_manager.h"
#else
#include "lib/architecture/i2c.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

typedef enum mcp4728Address {
    mcp4728Address0 = 0b1100000 << 1,
    mcp4728Address1 = 0b1100001 << 1,
    mcp4728Address2 = 0b1100010 << 1,
    mcp4728Address3 = 0b1100011 << 1,
    mcp4728Address4 = 0b1100100 << 1,
    mcp4728Address5 = 0b1100101 << 1,
    mcp4728Address6 = 0b1100110 << 1,
    mcp4728Address7 = 0b1100111 << 1,
    mcp4728AddressAuto,
} mcp4728Address_t;

#if defined(MCP4728_USE_I2C_MANAGER)
typedef struct mcp4728 {
    uint8_t deviceAddress;
    i2cManager_t *i2cManager;
    uint8_t buffer[8];
} mcp4728_t;
#else
typedef struct mcp4728 {
    uint8_t deviceAddress;
    I2C_HandleTypeDef* hi2c;
} mcp4728_t;
#endif

#if defined(MCP4728_USE_I2C_MANAGER)
extern void mcp4728Init(mcp4728_t *self, mcp4728Address_t deviceAddress, i2cManager_t *i2cManager);
#else
extern void mcp4728Init(mcp4728_t *self, mcp4728Address_t deviceAddress, I2C_HandleTypeDef *hi2c);
#endif

extern void mcp4728SetVoltages(mcp4728_t *self, uint16_t A, uint16_t B, uint16_t C, uint16_t D);

#ifdef __cplusplus
}
#endif
#endif //DRIVERS_DAC_MCP4728_H
