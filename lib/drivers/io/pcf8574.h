#ifndef DRIVERS_IO_PCF8574H_
#define DRIVERS_IO_PCF8574H_

/**
 ***********************************************************************************************************************
 * @file    pcf8574.h
 * @author  José Roberto Colombo Junior
 * @brief   Header file of the PCF8574 driver.
 ***********************************************************************************************************************
 *
 * This library adds support the I/O expander PCF8574. It is assumed that the interrupt pin (INT) is handled externally.
 *
 * @note The pin number notation is used here because it is easy to obtain the pin position from the number, for example
 *       by doing (1 << pinNumber).
 *
 * @attention
 *
 * This is free software licensed under GPL.
 *
 ***********************************************************************************************************************
 */

#include "lib/architecture/i2c.h"
#include "lib/utils/types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Defines all the possible addresses of PCF8574.
 * @note See sections 8.3.2 and 8.3.3 of the datasheet.
 */
typedef enum pcf8574PossibleAddresses {
    pcf8574Address0 = 0b0100000 << 1,  ///< Address with A2 = 0, A1 = 0 and A0 = 0.
    pcf8574Address1 = 0b0100001 << 1,  ///< Address with A2 = 0, A1 = 0 and A0 = 1.
    pcf8574Address2 = 0b0100010 << 1,  ///< Address with A2 = 0, A1 = 1 and A0 = 0.
    pcf8574Address3 = 0b0100011 << 1,  ///< Address with A2 = 0, A1 = 1 and A0 = 1.
    pcf8574Address4 = 0b0100100 << 1,  ///< Address with A2 = 1, A1 = 0 and A0 = 0.
    pcf8574Address5 = 0b0100101 << 1,  ///< Address with A2 = 1, A1 = 0 and A0 = 1.
    pcf8574Address6 = 0b0100110 << 1,  ///< Address with A2 = 1, A1 = 1 and A0 = 0.
    pcf8574Address7 = 0b0100111 << 1,  ///< Address with A2 = 1, A1 = 1 and A0 = 1.
} pcf8574PossibleAddresses;

/**
 * @brief Defines all possible modes to the PCF8574. It is worth noting that pull-up resistors are not supported by
 *        internal hardware of PCF8574, but they may be added externally.
 * @note See section 5 of the datasheet: "P-port input/output. Push-pull design structure".
 */
typedef enum pcf8574Mode {
    pcf8574ModeInput = 0,       ///< Pin works as input.
    pcf8574ModeOutput,          ///< Pin works as output.
} pcf8574Mode;

/**
 * A PCF8574 configuration model. You should instantiate one struct for each chip.
 */
typedef struct pcf8574 {
    I2C_HandleTypeDef* hi2c;     ///< I2C handler.
    uint8_t address;             ///< PCF8574 I2C address.
    uint8_t readBuffer;          ///< Buffer to read values.
    uint8_t writeBuffer;         ///< Buffer to write values.
    uint8_t timeout;             ///< I2C communication timeout.
} pcf8574;

/**
 * @brief Initialize a PCF8574 chip.
 * @param self a pointer to a struct containing the model the chip. One struct per chip.
 * @param i2cHandler a pointer to the I2C controller.
 * @param chipAddress the address of the chip.
 * @return true if error.
 */
bool pcf8574Init(pcf8574* self, I2C_HandleTypeDef* i2cHandler, uint8_t chipAddress);

/**
 * @brief Updates the chip registers.
 * @param self a pointer to a struct containing the model the chip.
 * @return true if error.
 */
bool pcf8574Commit(pcf8574* self);

/**
 * @brief Read the chip registers.
 * @param self a pointer to a struct containing the model the chip.
 * @return true if error.
 */
bool pcf8574ReadRegs(pcf8574* self);

/**
 * @brief Configure a pin of the chip.
 * @param self a pointer to a struct containing the model the chip.
 * @param pinNumber the pin number to be configured. This number must be between 0 and 7.
 * @param mode the pin operation mode.
 * @return true if error.
 * @see pcf8574Mode
 */
bool pcf8574PinMode(pcf8574* self, uint8_t pinNumber, pcf8574Mode mode);

/**
 * @brief Updates the pin output logic state immediately, i.e., it makes a communication transaction with the chip.
 * @param a pointer to a struct containing the model the chip.
 * @param pinNumber the pin number to be configured. This number must be between 0 and 7.
 * @param value the new logic state of the pin. It can 1 or 0.
 * @return true if error.
 */
bool pcf8574Write(pcf8574* self, uint8_t pinNumber, uint8_t value);

/**
 * @brief Updates the pin output logic state in buffer only, i.e., do not communicate with the chip. This is useful
 *        when changing the state of multiples pin at once. It also helps in synchronization purposes.
 * @param a pointer to a struct containing the model the chip.
 * @param pinNumber the pin number to be configured. This number must be between 0 and 7.
 * @param value the new logic state of the pin. It can 1 or 0.
 */
void pcf8574WriteToBuffer(pcf8574* self, uint8_t pinNumber, uint8_t value);

/**
 * @brief Read the pin logic state immediately, i.e., it makes a communication transaction with the chip.
 * @param a pointer to a struct containing the model the chip.
 * @param pinNumber the pin number to be configured. This number must be between 0 and 7.
 * @return the logic state of the pin, 1 or 0.
 */
uint8_t pcf8574Read(pcf8574* self, uint8_t pinNumber);

/**
 * @brief Read the pin logic state stored in buffer, i.e., it does not communicate with the chip. It is useful when
 *        reading multiple pins.
 * @param a pointer to a struct containing the model the chip.
 * @param pinNumber the pin number to be configured. This number must be between 0 and 7.
 * @return the logic state of the pin, 1 or 0.
 */
uint8_t pcf8574ReadFromBuffer(pcf8574* self, uint8_t pinNumber);

#ifdef __cplusplus
}
#endif

#endif /* DRIVERS_IO_PCF8574H_ */
