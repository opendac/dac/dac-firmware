#include "lib/drivers/io/pcf8574.h"
#include "lib/utils/bit.h"

bool pcf8574Init(pcf8574* self, I2C_HandleTypeDef* i2cHandler, uint8_t chipAddress)
{
    self->hi2c = i2cHandler;
    self->address = chipAddress;
    self->readBuffer = 0;
    self->writeBuffer = 0;
    self->timeout = 1;
    return pcf8574Commit(self);
}

bool pcf8574Commit(pcf8574* self)
{
    HAL_StatusTypeDef status = HAL_I2C_Master_Transmit(self->hi2c, self->address, &self->writeBuffer, 1, self->timeout);
    return status != HAL_OK;
}

bool pcf8574ReadRegs(pcf8574* self)
{
    HAL_StatusTypeDef status = HAL_I2C_Master_Receive(self->hi2c, self->address, &self->readBuffer, 1, self->timeout);
    return status != HAL_OK;
}

bool pcf8574PinMode(pcf8574* self, uint8_t pinNumber, pcf8574Mode mode)
{
    if (mode == pcf8574ModeOutput)
    {
        self->writeBuffer |= numberToBit(pinNumber);
    }
    else
    {
        self->writeBuffer &= (~numberToBit(pinNumber));
    }

    return pcf8574Commit(self);
}

bool pcf8574Write(pcf8574* self, uint8_t pinNumber, uint8_t value)
{
    pcf8574WriteToBuffer(self, pinNumber, value);
    return pcf8574Commit(self);
}

void pcf8574WriteToBuffer(pcf8574* self, uint8_t pinNumber, uint8_t value)
{
    value = (uint8_t) value > 0;
    self->writeBuffer &= ~(1 << pinNumber);
    self->writeBuffer |= (value << pinNumber);
}

uint8_t pcf8574Read(pcf8574* self, uint8_t pinNumber)
{
    pcf8574ReadRegs(self);
    return pcf8574ReadFromBuffer(self, pinNumber);
}

uint8_t pcf8574ReadFromBuffer(pcf8574* self, uint8_t pinNumber)
{
    return (self->readBuffer & numberToBit(pinNumber)) > 0;
}
