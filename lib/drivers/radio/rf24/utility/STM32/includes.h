/**
 * @file includes.h
 * Configuration defines for RF24/Linux
 */

/**
 * Example of includes.h for RF24 Linux portability
 *
 * @defgroup Porting_Includes Porting: Includes
 * @{
 */

#ifndef RF24_UTILITY_STM32_INCLUDES_H_
#define RF24_UTILITY_STM32_INCLUDES_H_

/**
 * Load the correct configuration for this platform
 */
#include "RF24_arch_config.h"

/**@}*/

#endif // RF24_UTILITY_STM32_INCLUDES_H_
