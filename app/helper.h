#ifndef DAQ_APP_HELPER_H_
#define DAQ_APP_HELPER_H_

/**
 ******************************************************************************
 * @file    helper.h
 * @author  José Roberto Colombo Junior
 * @brief   Header file of the helper functions set.
 ******************************************************************************
 * @attention
 *
 * This is free software licensed under GPL.
 *
 ******************************************************************************
 */

#include "lib/utils/types.h"
#include "lib/utils/errors.h"
#include "lib/communication/protocol.h"
#include "app/config.h"

#ifdef __cplusplus
extern "C" {
#endif
    extern void helperGetEncoderConfig(uint8_t encoderNumber, EncoderMode *mode, EncoderDirection *dir, EncoderResolution *resolution, uint8_t *filter);
    extern void daqInit();
    extern void startAdc();
    extern void helperMemoryInit();
    extern void helperMemoryUpdate();
    extern bool processPacket();
    extern void protocolMakeResponseForPing();
    extern void protocolMakeAutoReadMsg();

#ifdef __cplusplus
}
#endif

#endif /* APPLICATION_HELPER_H_ */
