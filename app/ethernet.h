#ifndef DAQ_APP_ETHERNET_H
#define DAQ_APP_ETHERNET_H

#include "lib/utils/errors.h"
#include "lib/utils/types.h"
#include "lib/drivers/ethernet/wizchip/Ethernet/wizchip_conf.h"
#include "lib/drivers/ethernet/wizchip/Ethernet/socket.h"

#ifdef __cplusplus
extern "C" {
#endif

    extern bool isIpValid(uint8_t *ip);
    extern error_t wizchipInit();
    extern void netPeriodic100ms();

#ifdef __cplusplus
}
#endif

#endif //DAQ_APP_ETHERNET_H
