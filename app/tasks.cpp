#include "usb_device.h"
#include "usbd_cdc_if.h"
#include "app/tasks.h"
#include "app/helper.h"
#include "app/global_objects.h"
#include "app/ethernet.h"
#include "Core/Inc/usart.h"

// Multiples ways to enable/disable interrupts in STM32 mcus
// https://stackoverflow.com/questions/71626597/what-are-the-various-ways-to-disable-and-re-enable-interrupts-in-stm32-microcont

#ifdef __cplusplus
extern "C" {
#endif

static void prvSendMsg(packetSource_t sendPacketThrough, commMsg* packet)
{
    uint8_t* addr = commMsgGetAddrOfPacket(packet);
    uint16_t size = commMsgGetTotalPacketSize(packet);

    switch (sendPacketThrough) {
        case sourceUsb:
            CDC_Transmit_FS(addr, size);
            break;

        case sourceUart:
            HAL_UART_Transmit_DMA(&huart2, addr, size);
            break;

        case sourceEthernet:
            send(tcpSocket, addr, size);
            break;

        case sourceRadio:
            radio.stopListening();
            radio.write(addr, size);
            radio.startListening();
            break;

        default:
            break;
    }
}

void taskCommunication()
{
    // Auxiliary variables
    int32_t len;
    bool generatedStatusPacket, tx_ok, tx_fail, rx_ready;

    // Handle bytes received through ethernet
    if (packetFrom == sourceEthernet)
    {
        // Copy data from the ethernet chip
        len = recv(tcpSocket, bufferRx, 256);
        if (len > 0)
        {
            commReceiverProcess(&receiver, bufferRx, len);
            packetFrom = sourceEthernet;
        }
    }

    if (packetFrom == sourceRadio)
    {
        // Clear the radio interrupt flags
        radio.whatHappened(tx_ok, tx_fail, rx_ready);
        if (radio.available())
        {
//            size = radio.getDynamicPayloadSize();
            radio.read(bufferRx, 32);
            commReceiverProcess(&receiver, bufferRx, 32);
        }
    }

    // If the new bytes formed a packet then add it to the queue
    if (commReceiverHasNewMessage(&receiver))
    {
        generatedStatusPacket = processPacket();
        if (generatedStatusPacket)
        {
            prvSendMsg(packetFrom, &statusPacket);
            packetFrom = sourceNone;
        }

        commReceiverClean(&receiver);
    }

    osSuspend(hTaskCommunication);
}

void taskAutoread()
{
    uint16_t periodMs;
    const uint32_t ti = getTimeUs();
    uint32_t elapsedTime, periodUs;

    // Build and send the message
    protocolMakeAutoReadMsg();
    prvSendMsg(autoReadTo, &autoReadPacket);

    // Wait until is ready to run
    if (table[AddressOfSetupAutoReadLength] > 0)
    {
        memcpy(&periodMs, &table[AddressOfSetupAutoReadPeriodMs], 2);
        periodUs = 1000 * periodMs;
        elapsedTime = getTimeUs() - ti;
        osDelayUs(hTaskAutoread, periodUs - elapsedTime);
    }
    else
    {
        autoReadTo = sourceNone;
        osSuspend(hTaskAutoread);
    }
}

void taskIdle()
{
    osDelayMs(hTaskIdle, 1);

    // Start A/D
    startAdc();

    if ((getTimeMs() % 100) == 0)
    {
        netPeriodic100ms();
    }
}

#ifdef __cplusplus
}
#endif
