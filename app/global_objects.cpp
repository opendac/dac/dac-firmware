#include "global_objects.h"

uint8_t table[256];

commMsg instructionPacket, statusPacket, autoReadPacket;
commReceiver receiver;
uint8_t bufferRx[256];
EthernetState netState;
wiz_NetInfo netInfo;
RF24 radio;
bool radioIsActive;
packetSource_t packetFrom, autoReadTo;

uint16_t enc1EventTime, enc1EventTimeOld;
uint16_t enc2EventTime, enc2EventTimeOld;
uint16_t enc3EventTime, enc3EventTimeOld;

osTask_t* hTaskCommunication;
osTask_t* hTaskAutoread;
osTask_t* hTaskIdle;

i2cManager_t i2cManager;
