#ifndef APP_CONFIG_H
#define APP_CONFIG_H

#include "lib/utils/types.h"
#include "Core/Inc/tim.h"
#include "lib/drivers/ethernet/wizchip/Ethernet/wizchip_conf.h"

// Default values in memory table
#define defaultValueVersion          1
#define defaultValueVersionWhoAmI 0x1D

// eeprom emulation
#define eepromBaseAddress 0x0801FC00

// Ethernet
#define tcpSocket           (0)
#define tcpPort          (1000)
#define tcpProtocol (Sn_MR_TCP)
#define socketFlag       (0x00)

// PWM definitions
#define PWM_TIMER (&htim4)
#define PWM_CH1   (TIM_CHANNEL_1)
#define PWM_CH2   (TIM_CHANNEL_2)
#define PWM_CH3   (TIM_CHANNEL_3)
#define PWM_CH4   (TIM_CHANNEL_4)

// Hard encoders
#define ENC1_TIMER (&htim1)
#ifndef GPIO_ENC1_CHB_GPIO_Port
#define GPIO_ENC1_CHB_GPIO_Port GPIOA
#define GPIO_ENC1_CHB_Pin       GPIO_PIN_9
#endif
#define ENC2_TIMER (&htim2)
#ifndef GPIO_ENC2_CHB_GPIO_Port
#define GPIO_ENC2_CHB_GPIO_Port GPIOB
#define GPIO_ENC2_CHB_Pin       GPIO_PIN_3
#endif
#define ENC3_TIMER (&htim3)
#ifndef GPIO_ENC3_CHB_GPIO_Port
#define GPIO_ENC3_CHB_GPIO_Port GPIOB
#define GPIO_ENC3_CHB_Pin       GPIO_PIN_5
#endif

typedef GPIO_PinState PinState;

typedef enum EthernetStatus {
    EthernetStatusUninitialized = 0,
    EthernetWaitForCable,
    EthernetStatusReady,
    EthernetStatusSize,
} EthernetState;

typedef enum EncoderMode {
    encoderModePulseCount = 0,
    encoderModeTimeMeasurement,
} EncoderMode;

typedef enum EncoderResolution {
    /**
     * In pulse count mode, this option implements the X2 quadrature, i.e., capture all edges of CHA
     * (it uses CHB only to decide CW or CCW direction).
     *
     * In time measurement mode, this option implements X1 quadrature, i.e., measure the time between two consecutive
     * rising edges on CHA. CHB is used only for deciding between CW or CCW.
     */
    LowResolution = 0,

    /**
     * In pulse count mode, this option implements the X4 quadrature, i.e., capture all edges of CHA and CHB.
     *
     * In time measurement mode, this option implements X2 quadrature, i.e., measure the time between rising and falling
     * edges on CHA. CHB is used only for deciding between CW or CCW.
     */
    HighResolution = 1,
} EncoderResolution;

typedef enum EncoderDirection {
    encoderDirectionCCW = 0,    ///< Encoder will increment +1 if running in counter-clockwise.
    encoderDirectionCW = 1,     ///< Encoder will increment +1 if running in clockwise.
} EncoderDirection;

//typedef enum pwmMode {
//} pwmMode;

/**
 * Registers addresses in the memory table.
 */
typedef enum MemoryTableAddress {
    AddressOfReadingsGpioInputState = 0,       ///< Memory table address of the gpio input state.
    AddressOfReadingsEncoder1 = 2,             ///< Memory table address of the encoder 1 data.
    AddressOfReadingsEncoder2 = 4,             ///< Memory table address of the encoder 2 data.
    AddressOfReadingsEncoder3 = 6,             ///< Memory table address of the encoder 3 data.
    AddressOfReadingsAdcCh1 = 8,               ///< Memory table address of the A/D channel 1 data.
    AddressOfReadingsAdcCh2 = 10,              ///< Memory table address of the A/D channel 2 data.
    AddressOfReadingsAdcCh3 = 12,              ///< Memory table address of the A/D channel 3 data.
    AddressOfReadingsAdcCh4 = 14,              ///< Memory table address of the A/D channel 4 data.
    AddressOfActionPwmDutyCh1 = 111,           ///< Memory table address of the pwm CH1 duty cycle.
    AddressOfActionPwmDutyCh2 = 113,           ///< Memory table address of the pwm CH2 duty cycle.
    AddressOfActionPwmDutyCh3 = 115,           ///< Memory table address of the pwm CH3 duty cycle.
    AddressOfActionPwmDutyCh4 = 117,           ///< Memory table address of the pwm CH4 duty cycle.
    AddressOfActionDacCh1 = 119,               ///< Memory table address of the dac CH1 data.
    AddressOfActionDacCh2 = 121,               ///< Memory table address of the dac CH2 data
    AddressOfActionDacCh3 = 123,               ///< Memory table address of the dac CH3 data
    AddressOfActionDacCh4 = 125,               ///< Memory table address of the dac CH4 data
    AddressOfActionGpioOutputState = 127,      ///< Memory table address of the gpio output state.

    AddressOfSetupAutoReadPeriodMs = 208,      ///< Memory table address of the auto read period (in milli-seconds)
    AddressOfSetupAutoReadFirstReg = 210,      ///< Memory table address of the auto read first register.
    AddressOfSetupAutoReadLength = 211,        ///< Memory table address of the auto read length.

    AddressOfSetupEncoder1 = 212,              ///< Memory table address of the encoder configuration.
    AddressOfSetupEncoder2 = 213,              ///< Memory table address of the encoder configuration.
    AddressOfSetupEncoder3 = 214,              ///< Memory table address of the encoder configuration.
    AddressOfSetupPwmPrescaler = 216,          ///< Memory table address of the pwm prescaler register, which controls pwm frequency.
    AddressOfSetupPwmMaxCount = 218,           ///< Memory table address of the max count register, which controls pwm frequency.
    AddressOfSetupPwmConfig = 220,             ///< Memory table address of the pwm config.

    AddressOfSetupBluetooth = 222,             ///< Memory table address of the bluetooth configuration.

    AddressOfSetupRadioConfig = 224,           ///< Memory table address of the radio configuration.
    AddressOfSetupRadioChannel = 225,
    AddressOfSetupRadioBoardAddress = 226,     ///< Memory table address of the radio own address.
    AddressOfSetupRadioPcAddress = 231,        ///< Memory table address of the radio master address.

    AddressOfSetupEthernetMac = 236,           ///< Memory table address of the board mac address.
    AddressOfSetupEthernetIp = 242,            ///< Memory table address of the board ip.
    AddressOfSetupEthernetNetMask = 246,       ///< Memory table address of the board subnet mask.
    AddressOfSetupEthernetGw = 250,            ///< Memory table address of the board gateway.

    AddressOfSetupVersion = 254,               ///< Memory table address of the firmware patch version.
    AddressOfSetupBoardId = 255,               ///< Memory table address of the board identification.
} MemoryTableAddress;

typedef enum packetSource {
    sourceNone = 0,
    sourceUsb,
    sourceUart,
    sourceEthernet,
    sourceRadio,
} packetSource_t;

#endif // APP_CONFIG_H
