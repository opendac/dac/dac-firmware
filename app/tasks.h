#ifndef APP_TASKS_H
#define APP_TASKS_H

#ifdef __cplusplus
extern "C" {
#endif

void taskCommunication();
void taskAutoread();
void taskIdle();

#ifdef __cplusplus
}
#endif

#endif //APP_TASKS_H
