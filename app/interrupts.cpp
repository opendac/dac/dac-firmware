#include "app/global_objects.h"
#include "Core/Inc/usart.h"

#ifdef __cplusplus
extern "C" {
#endif

void HAL_UARTEx_RxEventCallback(UART_HandleTypeDef* huart, uint16_t size)
{
    // Process incoming bytes
    commReceiverProcess(&receiver, bufferRx, size);

    // Trigger a new data receive operation
    // https://electronics.stackexchange.com/questions/571422/determining-how-many-bytes-dma-receives-with-hal-uart-receive-dma
    HAL_UARTEx_ReceiveToIdle_DMA(huart, bufferRx, 256);
    __HAL_DMA_DISABLE_IT(huart2.hdmarx, DMA_IT_HT);

    // If required, unblock the communication task
    if (commReceiverHasNewMessage(&receiver))
    {
        packetFrom = sourceUart;
        osResume(hTaskCommunication);
    }
}

void interruptUsb(uint8_t *buffer, uint32_t length)
{
    // Process incoming data
    commReceiverProcess(&receiver, buffer, length);

    // If the new bytes formed a packet then add it to the queue
    if (commReceiverHasNewMessage(&receiver))
    {
        // Unblock the communication task
        packetFrom = sourceUsb;
        osResume(hTaskCommunication);
    }
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
    // Auxiliary variables
    uint8_t interruptInfo;

    if (radioIsActive)
    {
        // Unblock the communication task
        packetFrom = sourceRadio;
        osResume(hTaskCommunication);
    }

    if (netState == EthernetStatusReady)
    {
        interruptInfo = getSn_IR(tcpSocket);
        if(interruptInfo & Sn_IR_CON)
        {
            // Clear interrupt
            setSn_IR(tcpSocket, Sn_IR_CON);
        }
        else if (interruptInfo & Sn_IR_RECV)
        {
            // Clear interrupt
            setSn_IR(tcpSocket, Sn_IR_RECV);

            // Unblock the communication task
            packetFrom = sourceEthernet;
            osResume(hTaskCommunication);
        }
        else if (interruptInfo & Sn_IR_DISCON)
        {
            // the client has disconnected
        }
    }

    UNUSED(GPIO_Pin);
}

// adc interrupt
//void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
//{
//    UNUSED(hadc);
//    table.Reg.Readings.Adc.Ch1 = adcReadings[0];
//    table.Reg.Readings.Adc.Ch2 = adcReadings[1];
//    table.Reg.Readings.Adc.Ch3 = adcReadings[2];
//    table.Reg.Readings.Adc.Ch4 = adcReadings[3];
//}

void HAL_TIM_IC_CaptureCallback(TIM_HandleTypeDef* htim)
{
    const uint8_t enc1Dir = bitRead(&table[AddressOfSetupEncoder1], 2, 0b1);
    const uint8_t enc2Dir = bitRead(&table[AddressOfSetupEncoder2], 2, 0b1);
    const uint8_t enc3Dir = bitRead(&table[AddressOfSetupEncoder3], 2, 0b1);
    uint8_t sign;

    if (htim == ENC1_TIMER)
    {
        enc1EventTimeOld = enc1EventTime;

        // Captured a rising edge
        if (htim->Channel == HAL_TIM_ACTIVE_CHANNEL_1)
        {
            sign = enc1Dir ^ (!gpioFastRead(GPIO_ENC1_CHB_GPIO_Port, GPIO_ENC1_CHB_Pin));
            enc1EventTime = ENC1_TIMER->Instance->CCR1;
        }
        // Captured a falling edge
        else // if (htim->Channel == HAL_TIM_ACTIVE_CHANNEL_2)
        {
            sign = enc1Dir ^ (gpioFastRead(GPIO_ENC1_CHB_GPIO_Port, GPIO_ENC1_CHB_Pin));
            enc1EventTime = ENC1_TIMER->Instance->CCR2;
        }
        bitWriteRw(&table[AddressOfReadingsGpioInputState], sign, 3, 0b1);
    }
    else if (htim == ENC2_TIMER)
    {
        enc2EventTimeOld = enc2EventTime;

        // Captured a rising edge
        if (htim->Channel == HAL_TIM_ACTIVE_CHANNEL_1)
        {
            sign = enc2Dir ^ (!gpioFastRead(GPIO_ENC2_CHB_GPIO_Port, GPIO_ENC2_CHB_Pin));
            enc2EventTime = ENC2_TIMER->Instance->CCR1;
        }
        // Captured a falling edge
        else // if (htim->Channel == HAL_TIM_ACTIVE_CHANNEL_2)
        {
            sign = enc2Dir ^ (gpioFastRead(GPIO_ENC2_CHB_GPIO_Port, GPIO_ENC2_CHB_Pin));
            enc2EventTime = ENC2_TIMER->Instance->CCR2;
        }
        bitWriteRw(&table[AddressOfReadingsGpioInputState], sign, 4, 0b1);
    }
    else if (htim == ENC3_TIMER)
    {
        enc3EventTimeOld = enc3EventTime;

        // Captured a rising edge
        if (htim->Channel == HAL_TIM_ACTIVE_CHANNEL_1)
        {
            sign = enc3Dir ^ (!gpioFastRead(GPIO_ENC3_CHB_GPIO_Port, GPIO_ENC3_CHB_Pin));
            enc3EventTime = ENC3_TIMER->Instance->CCR1;
        }
        // Captured a falling edge
        else // if (htim->Channel == HAL_TIM_ACTIVE_CHANNEL_2)
        {
            sign = enc3Dir ^ (gpioFastRead(GPIO_ENC3_CHB_GPIO_Port, GPIO_ENC3_CHB_Pin));
            enc3EventTime = ENC3_TIMER->Instance->CCR2;
        }
        bitWriteRw(&table[AddressOfReadingsGpioInputState], sign, 5, 0b1);
    }
}

void HAL_I2C_MasterTxCpltCallback(I2C_HandleTypeDef *hi2c)
{
    UNUSED(hi2c);
    i2cManagerCallbackFinished(&i2cManager);
}

void HAL_I2C_MasterRxCpltCallback(I2C_HandleTypeDef *hi2c)
{
    UNUSED(hi2c);
    i2cManagerCallbackFinished(&i2cManager);
}

void HAL_I2C_MemTxCpltCallback(I2C_HandleTypeDef *hi2c)
{
    UNUSED(hi2c);
    i2cManagerCallbackFinished(&i2cManager);
}

void HAL_I2C_MemRxCpltCallback(I2C_HandleTypeDef *hi2c)
{
    UNUSED(hi2c);
    i2cManagerCallbackFinished(&i2cManager);
}

void HAL_I2C_ErrorCallback(I2C_HandleTypeDef *hi2c)
{
    UNUSED(hi2c);
    i2cManagerCallbackFinished(&i2cManager);
}

#ifdef __cplusplus
}
#endif
