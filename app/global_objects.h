#ifndef DAQ_APP_GLOBAL_OBJECTS_H_
#define DAQ_APP_GLOBAL_OBJECTS_H_

#include "Core/Inc/i2c.h"
#include "Core/Inc/spi.h"
#include "Core/Inc/tim.h"

#include "app/config.h"
#include "lib/communication/protocol.h"
#include "lib/communication/receiver.h"
#include "lib/utils/fifo.h"
#include "lib/drivers/sensors/soft_encoder.h"
#include "lib/drivers/ethernet/wizchip/Ethernet/wizchip_conf.h"
#include "lib/drivers/radio/rf24/RF24.h"
#include "lib/drivers/hal/i2c_manager.h"
#include "lib/os/os.h"
#include "app/config.h"

extern uint8_t table[256];

extern commMsg instructionPacket, statusPacket, autoReadPacket;
extern commReceiver receiver;
extern uint8_t bufferRx[256];
extern EthernetState netState;
extern wiz_NetInfo netInfo;
extern RF24 radio;
extern bool radioIsActive;
extern packetSource_t packetFrom, autoReadTo;

extern uint16_t enc1EventTime, enc1EventTimeOld;
extern uint16_t enc2EventTime, enc2EventTimeOld;
extern uint16_t enc3EventTime, enc3EventTimeOld;

extern osTask_t* hTaskCommunication;
extern osTask_t* hTaskAutoread;
extern osTask_t* hTaskIdle;

extern i2cManager_t i2cManager;

#endif /* DAQ_APP_GLOBAL_OBJECTS_H_ */
