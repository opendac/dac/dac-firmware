#include "helper.h"
#include "Core/Inc/adc.h"
#include "Core/Inc/spi.h"
#include "Core/Inc/usart.h"
#include "app/global_objects.h"
#include "app/ethernet.h"
#include "app/tasks.h"
#include <memory.h>

static constexpr uint8_t  eepromTotalSizeInBytes     = 36;
static constexpr uint8_t  eepromFirstVirtualAddress  = AddressOfSetupPwmConfig;
static constexpr uint32_t eepromFirstRawAddress      = eepromBaseAddress + eepromFirstVirtualAddress;


constexpr uint8_t registerSize[256] = {
        1,   // Gpio input readings
        0,   // Reserved
        2,   // Readings, encoder 1
        2,   // Readings, encoder 1
        2,   // Readings, encoder 2
        2,   // Readings, encoder 2
        2,   // Readings, encoder 3
        2,   // Readings, encoder 3
        2,   // Readings, A/D CH1
        2,   // Readings, A/D CH1
        2,  // Readings, A/D CH2
        2,  // Readings, A/D CH2
        2,  // Readings, A/D CH3
        2,  // Readings, A/D CH3
        2,  // Readings, A/D CH4
        2,  // Readings, A/D CH4
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        2,  // Action, pwm duty cycle CH1
        2,  // Action, pwm duty cycle CH1
        2,  // Action, pwm duty cycle CH2
        2,  // Action, pwm duty cycle CH2
        2,  // Action, pwm duty cycle CH3
        2,  // Action, pwm duty cycle CH3
        2,  // Action, pwm duty cycle CH4
        2,  // Action, pwm duty cycle CH4
        2,  // Action, D/A CH1
        2,  // Action, D/A CH1
        2,  // Action, D/A CH2
        2,  // Action, D/A CH2
        2,  // Action, D/A CH3
        2,  // Action, D/A CH3
        2,  // Action, D/A CH4
        2,  // Action, D/A CH4
        1,  // Action, gpio output
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        2,  // Auto read, period in milli-seconds
        2,  // Auto read, period in milli-seconds
        1,  // Auto read, first register
        1,  // Auto read, length
        1,  // Encoder 1 config
        1,  // Encoder 2 config
        1,  // Encoder 3 config
        0,  // Reserved
        2,  // Pwm prescaler
        2,  // Pwm prescaler
        2,  // Pwm max count
        2,  // Pwm max count
        2,  // Pwm config
        2,  // Pwm config
        2,  // Bluetooth, config
        2,  // Bluetooth, config
        1,  // Radio, config
        1,  // Radio, channel
        5,  // Radio, own address
        5,  // Radio, own address
        5,  // Radio, own address
        5,  // Radio, own address
        5,  // Radio, own address
        5,  // Radio, master address
        5,  // Radio, master address
        5,  // Radio, master address
        5,  // Radio, master address
        5,  // Radio, master address
        6,  // MAC address
        6,  // MAC address
        6,  // MAC address
        6,  // MAC address
        6,  // MAC address
        6,  // MAC address
        4,  // Ethernet, IP
        4,  // Ethernet, IP
        4,  // Ethernet, IP
        4,  // Ethernet, IP
        4,  // Ethernet, subnet mask
        4,  // Ethernet, subnet mask
        4,  // Ethernet, subnet mask
        4,  // Ethernet, subnet mask
        4,  // Ethernet, gateway
        4,  // Ethernet, gateway
        4,  // Ethernet, gateway
        4,  // Ethernet, gateway
        1,  // Version
        1   // Board id
};

void helperGetEncoderConfig(uint8_t encoderNumber, EncoderMode *mode, EncoderDirection *dir, EncoderResolution *resolution, uint8_t *filter)
{
    switch (encoderNumber) {
        case 1:
            *mode = static_cast<EncoderMode>(bitRead(&table[AddressOfSetupEncoder1], 0, 1));
            *dir = static_cast<EncoderDirection>(bitRead(&table[AddressOfSetupEncoder1], 2, 1));
            *resolution = static_cast<EncoderResolution>(bitRead(&table[AddressOfSetupEncoder1], 1, 1));
            *filter = bitRead(&table[AddressOfSetupEncoder1], 4, 15);
            break;

        case 2:
            *mode = static_cast<EncoderMode>(bitRead(&table[AddressOfSetupEncoder2], 0, 1));
            *dir = static_cast<EncoderDirection>(bitRead(&table[AddressOfSetupEncoder2], 2, 1));
            *resolution = static_cast<EncoderResolution>(bitRead(&table[AddressOfSetupEncoder2], 1, 1));
            *filter = bitRead(&table[AddressOfSetupEncoder2], 4, 15);
            break;

        case 3:
            *mode = static_cast<EncoderMode>(bitRead(&table[AddressOfSetupEncoder3], 0, 1));
            *dir = static_cast<EncoderDirection>(bitRead(&table[AddressOfSetupEncoder3], 2, 1));
            *resolution = static_cast<EncoderResolution>(bitRead(&table[AddressOfSetupEncoder3], 1, 1));
            *filter = bitRead(&table[AddressOfSetupEncoder3], 4, 15);
            break;

        default:
            break;
    }
}

static void helperI2cInit()
{
    i2cManagerInit(&i2cManager, &hi2c2);
}

static void helperEncoderInit(TIM_HandleTypeDef *htim, EncoderMode mode, EncoderResolution resolution)
{
    if (mode == encoderModePulseCount)
    {
        HAL_TIM_Encoder_Start(htim, TIM_CHANNEL_ALL);
    }
    else
    {
        HAL_TIM_IC_Start_IT(htim, TIM_CHANNEL_1);
        if (resolution == HighResolution)
            HAL_TIM_IC_Start_IT(htim, TIM_CHANNEL_2);
    }
}

static void helperEncoderStop(TIM_HandleTypeDef *htim, EncoderMode mode, EncoderResolution resolution)
{
    if (mode == encoderModePulseCount)
    {
        HAL_TIM_Encoder_Stop(htim, TIM_CHANNEL_ALL);
        HAL_TIM_Encoder_DeInit(htim);
    }
    else
    {
        HAL_TIM_IC_Stop_IT(htim, TIM_CHANNEL_1);
        if (resolution == HighResolution)
            HAL_TIM_IC_Stop_IT(htim, TIM_CHANNEL_2);
        HAL_TIM_Base_Stop(htim);
        HAL_TIM_IC_DeInit(htim);
        HAL_TIM_Base_DeInit(htim);
    }
}

void startAdc()
{
    HAL_ADC_Start_DMA(&hadc1, (uint32_t*) &table[AddressOfReadingsAdcCh1], hadc1.Init.NbrOfConversion);
}

static void helperForceGpioReadAll()
{
    const PinState ch1 = HAL_GPIO_ReadPin(GPIO_IN_CH1_GPIO_Port, GPIO_IN_CH1_Pin);
    const PinState ch2 = HAL_GPIO_ReadPin(GPIO_IN_CH2_GPIO_Port, GPIO_IN_CH2_Pin);
    const PinState ch3 = HAL_GPIO_ReadPin(GPIO_IN_CH3_GPIO_Port, GPIO_IN_CH3_Pin);
    const uint8_t all = (ch3 << 2) | (ch2 << 1) | (ch1 << 0);
    bitWriteRw(&table[AddressOfReadingsGpioInputState], all, 0, 0b111);
}

void helperGpioInit()
{
    // Force a gpio input read
    helperForceGpioReadAll();
}

void helperGpioUpdateOutputState()
{
    const auto ch1 = static_cast<GPIO_PinState>(bitRead(&table[AddressOfActionGpioOutputState], 0, 1));
    const auto ch2 = static_cast<GPIO_PinState>(bitRead(&table[AddressOfActionGpioOutputState], 1, 1));
    const auto ch3 = static_cast<GPIO_PinState>(bitRead(&table[AddressOfActionGpioOutputState], 2, 1));
    HAL_GPIO_WritePin(GPIO_OUT_CH1_GPIO_Port, GPIO_OUT_CH1_Pin, ch1);
    HAL_GPIO_WritePin(GPIO_OUT_CH2_GPIO_Port, GPIO_OUT_CH2_Pin, ch2);
    HAL_GPIO_WritePin(GPIO_OUT_CH3_GPIO_Port, GPIO_OUT_CH3_Pin, ch3);
}

void helperEepromLoad()
{
    uint16_t i;
    uint32_t data;

    HAL_FLASH_Unlock();

    // Flash must be in erase state to be programmed
    FLASH_EraseInitTypeDef pEraseInit = {
            .TypeErase = FLASH_TYPEERASE_PAGES,
            .Banks = 0,
            .PageAddress = eepromBaseAddress,
            .NbPages = 1
    };
    uint32_t pageError;
    HAL_StatusTypeDef status = HAL_FLASHEx_Erase(&pEraseInit, &pageError);
    UNUSED(status);

    for (i = eepromFirstVirtualAddress; i < 256; i = i + 4)
    {
        memcpy(&data, &table[i], 4);
        HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, eepromBaseAddress + i, data);
    }

    HAL_FLASH_Lock();
}

static void helperEepromInit()
{
    // persistent memory
    memcpy(&table[eepromFirstVirtualAddress], reinterpret_cast<uint8_t*>(eepromFirstRawAddress), eepromTotalSizeInBytes);
    if (table[AddressOfSetupBoardId] != defaultValueVersionWhoAmI)
    {
        // Make sure this region is filled with zeros
        memset(&table[eepromFirstVirtualAddress], 0, eepromTotalSizeInBytes);

        // Then persistent memory is not initialized or it got corrupted
        table[AddressOfSetupRadioChannel] = 113;
        bitWriteRw(&table[AddressOfSetupRadioConfig], 1, 0, 0b11);    // Data rate = RF24_2MBPS
        bitWriteRw(&table[AddressOfSetupRadioConfig], 2, 2, 0b11);    // PA level = RF24_PA_HIGH
        table[AddressOfSetupRadioBoardAddress + 0] = 'R';             // The board address is RXaaa
        table[AddressOfSetupRadioBoardAddress + 1] = 'X';             // The board address is RXaaa
        table[AddressOfSetupRadioBoardAddress + 2] = 'a';             // The board address is RXaaa
        table[AddressOfSetupRadioBoardAddress + 3] = 'a';             // The board address is RXaaa
        table[AddressOfSetupRadioBoardAddress + 4] = 'a';             // The board address is RXaaa
        table[AddressOfSetupRadioPcAddress + 0] = 'T';                // The computer address is TXaaa
        table[AddressOfSetupRadioPcAddress + 1] = 'X';                // The computer address is TXaaa
        table[AddressOfSetupRadioPcAddress + 2] = 'a';                // The computer address is TXaaa
        table[AddressOfSetupRadioPcAddress + 3] = 'a';                // The computer address is TXaaa
        table[AddressOfSetupRadioPcAddress + 4] = 'a';                // The computer address is TXaaa

        table[AddressOfSetupEthernetMac + 0] = 0x9E;
        table[AddressOfSetupEthernetMac + 1] = 0x7E;
        table[AddressOfSetupEthernetMac + 2] = 0x87;
        table[AddressOfSetupEthernetMac + 3] = 0x5C;
        table[AddressOfSetupEthernetMac + 4] = 0x05;
        table[AddressOfSetupEthernetMac + 5] = 0x1E;

        table[AddressOfSetupEthernetIp + 0] = 192;
        table[AddressOfSetupEthernetIp + 1] = 168;
        table[AddressOfSetupEthernetIp + 2] = 0;
        table[AddressOfSetupEthernetIp + 3] = 2;

        table[AddressOfSetupEthernetNetMask + 0] = 255;
        table[AddressOfSetupEthernetNetMask + 1] = 255;
        table[AddressOfSetupEthernetNetMask + 2] = 255;
        table[AddressOfSetupEthernetNetMask + 3] = 0;

        table[AddressOfSetupEthernetGw + 0] = 192;
        table[AddressOfSetupEthernetGw + 1] = 168;
        table[AddressOfSetupEthernetGw + 2] = 0;
        table[AddressOfSetupEthernetGw + 3] = 1;

        table[AddressOfSetupVersion] = defaultValueVersion;
        table[AddressOfSetupBoardId] = 29;

        helperEepromLoad();
    }
}

void helperMemoryInit()
{
    // Initialize the memory table with zeros
    memset(table, 0, 256);

    // Copy non-zero values
    memcpy(&table[AddressOfSetupPwmMaxCount], &PWM_TIMER->Init.Period, 2);
    memcpy(&table[AddressOfSetupPwmPrescaler], &PWM_TIMER->Init.Prescaler, 2);
    table[AddressOfSetupEncoder1] = 0b11110110;  // Filter = 15, CW, high resolution, pulse count
    table[AddressOfSetupEncoder2] = 0b11110110;  // Filter = 15, CW, high resolution, pulse count
    table[AddressOfSetupEncoder3] = 0b11110110;  // Filter = 15, CW, high resolution, pulse count
}

void helperMemoryUpdate()
{
    uint16_t value;
//    const uint32_t now = getTimeUs();
    helperForceGpioReadAll();

    if (static_cast<EncoderMode>(bitRead(&table[AddressOfSetupEncoder1], 0, 1)) == encoderModePulseCount)
    {
        value = __HAL_TIM_GET_COUNTER(ENC1_TIMER);
    }
    else
    {
        value = enc1EventTime - enc1EventTimeOld + ((enc1EventTime > enc1EventTimeOld) ? 0 : 0xFFFF);
    }
    memcpy(&table[AddressOfReadingsEncoder1], &value, 2);

    if (static_cast<EncoderMode>(bitRead(&table[AddressOfSetupEncoder2], 0, 1)) == encoderModePulseCount)
    {
        value = __HAL_TIM_GET_COUNTER(ENC2_TIMER);
    }
    else
    {
        value = enc2EventTime - enc2EventTimeOld + ((enc2EventTime > enc2EventTimeOld) ? 0 : 0xFFFF);
//        if (now - htim2LastCaptureUs > (2500 * table.Reg.Setup.AutoRead.periodMs))
//            table.Reg.Readings.encoder2 += (now - htim2LastCaptureUs) / 100;
    }
    memcpy(&table[AddressOfReadingsEncoder2], &value, 2);

    if (static_cast<EncoderMode>(bitRead(&table[AddressOfSetupEncoder3], 0, 1)) == encoderModePulseCount)
    {
        value = __HAL_TIM_GET_COUNTER(ENC3_TIMER);
    }
    else
    {
        value = enc3EventTime - enc3EventTimeOld + ((enc3EventTime > enc3EventTimeOld) ? 0 : 0xFFFF);
    }
    memcpy(&table[AddressOfReadingsEncoder3], &value, 2);
}

void helperPwmInit()
{
    HAL_TIM_Base_Start(PWM_TIMER);
    HAL_TIM_PWM_Start(PWM_TIMER, PWM_CH1);
    HAL_TIM_PWM_Start(PWM_TIMER, PWM_CH2);
    HAL_TIM_PWM_Start(PWM_TIMER, PWM_CH3);
    HAL_TIM_PWM_Start(PWM_TIMER, PWM_CH4);
}

static void helperRestartEthernet()
{
    if (netState != EthernetStatusUninitialized)
    {
        if (getSn_SR(tcpSocket) != SOCK_CLOSED)
        {
            close(tcpSocket);
        }
    }

    netState = EthernetStatusUninitialized;
}

static void helperProtocolProcessWriteInstruction(uint16_t addr, int size, const uint8_t* values)
{
    uint16_t first_address = addr;
    uint8_t sizeInBytes;
    int32_t length = size;
    uint16_t u16;
    EncoderMode mode;
    EncoderResolution resolution;

    while (length > 0) {
        switch (addr) {
            case AddressOfReadingsEncoder1:
                sizeInBytes = registerSize[AddressOfReadingsEncoder1];
                memcpy(&u16, &values[addr - first_address], sizeInBytes);
                memcpy(&table[AddressOfReadingsEncoder1], &values[addr - first_address], sizeInBytes);
                __HAL_TIM_SET_COUNTER(ENC1_TIMER, u16);
                length = length - sizeInBytes;
                addr = addr + sizeInBytes;
                break;

            case AddressOfReadingsEncoder2:
                sizeInBytes = registerSize[AddressOfReadingsEncoder2];
                memcpy(&u16, &values[addr - first_address], sizeInBytes);
                memcpy(&table[AddressOfReadingsEncoder2], &values[addr - first_address], sizeInBytes);
                __HAL_TIM_SET_COUNTER(ENC2_TIMER, u16);
                length = length - sizeInBytes;
                addr = addr + sizeInBytes;
                break;

            case AddressOfReadingsEncoder3:
                sizeInBytes = registerSize[AddressOfReadingsEncoder3];
                memcpy(&u16, &values[addr - first_address], sizeInBytes);
                memcpy(&table[AddressOfReadingsEncoder3], &values[addr - first_address], sizeInBytes);
                __HAL_TIM_SET_COUNTER(ENC3_TIMER, u16);
                length = length - sizeInBytes;
                addr = addr + sizeInBytes;
                break;

            case AddressOfActionPwmDutyCh1:
                sizeInBytes = registerSize[AddressOfActionPwmDutyCh1];
                memcpy(&u16, &values[addr - first_address], sizeInBytes);
                memcpy(&table[AddressOfActionPwmDutyCh1], &values[addr - first_address], sizeInBytes);
                PWM_TIMER->Instance->CCR1 = u16;
                length = length - sizeInBytes;
                addr = addr + sizeInBytes;
                break;

            case AddressOfActionPwmDutyCh2:
                sizeInBytes = registerSize[AddressOfActionPwmDutyCh2];
                memcpy(&u16, &values[addr - first_address], sizeInBytes);
                memcpy(&table[AddressOfActionPwmDutyCh2], &values[addr - first_address], sizeInBytes);
                PWM_TIMER->Instance->CCR2 = u16;
                length = length - sizeInBytes;
                addr = addr + sizeInBytes;
                break;

            case AddressOfActionPwmDutyCh3:
                sizeInBytes = registerSize[AddressOfActionPwmDutyCh3];
                memcpy(&u16, &values[addr - first_address], sizeInBytes);
                memcpy(&table[AddressOfActionPwmDutyCh3], &values[addr - first_address], sizeInBytes);
                PWM_TIMER->Instance->CCR3 = u16;
                length = length - sizeInBytes;
                addr = addr + sizeInBytes;
                break;

            case AddressOfActionPwmDutyCh4:
                sizeInBytes = registerSize[AddressOfActionPwmDutyCh4];
                memcpy(&u16, &values[addr - first_address], sizeInBytes);
                memcpy(&table[AddressOfActionPwmDutyCh4], &values[addr - first_address], sizeInBytes);
                PWM_TIMER->Instance->CCR4 = u16;
                length = length - sizeInBytes;
                addr = addr + sizeInBytes;
                break;

            case AddressOfActionGpioOutputState:
                sizeInBytes = registerSize[AddressOfActionGpioOutputState];
                table[AddressOfActionGpioOutputState] = values[addr - first_address];
                helperGpioUpdateOutputState();
                length = length - sizeInBytes;
                addr = addr + sizeInBytes;
                break;

            case AddressOfSetupAutoReadPeriodMs:
                sizeInBytes = registerSize[AddressOfSetupAutoReadPeriodMs];
                memcpy(&u16, &values[addr - first_address], sizeInBytes);
                memcpy(&table[AddressOfSetupAutoReadPeriodMs], &values[addr - first_address], sizeInBytes);
                length = length - sizeInBytes;
                addr = addr + sizeInBytes;
                break;

            case AddressOfSetupAutoReadFirstReg:
                sizeInBytes = registerSize[AddressOfSetupAutoReadFirstReg];
                table[AddressOfSetupAutoReadFirstReg] = values[addr - first_address];
                length = length - sizeInBytes;
                addr = addr + sizeInBytes;
                break;

            case AddressOfSetupAutoReadLength:
                sizeInBytes = registerSize[AddressOfSetupAutoReadLength];
                table[AddressOfSetupAutoReadLength] = values[addr - first_address];
                if (table[AddressOfSetupAutoReadLength] > 0)
                {
                    autoReadTo = packetFrom;
                    osResume(hTaskAutoread);
                }
                length = length - sizeInBytes;
                addr = addr + sizeInBytes;
                break;

            case AddressOfSetupEncoder1:
                sizeInBytes = registerSize[AddressOfSetupEncoder1];
                mode = static_cast<EncoderMode>(bitRead(&table[AddressOfSetupEncoder1], 0, 1));
                resolution = static_cast<EncoderResolution>(bitRead(&table[AddressOfSetupEncoder1], 1, 1));
                helperEncoderStop(ENC1_TIMER, mode, resolution);

                // Copy new configuration
                table[AddressOfSetupEncoder1] = values[addr - first_address];
                length = length - sizeInBytes;
                addr = addr + sizeInBytes;

                // Apply new configuration
                MX_TIM1_Init();
                mode = static_cast<EncoderMode>(bitRead(&table[AddressOfSetupEncoder1], 0, 1));
                resolution = static_cast<EncoderResolution>(bitRead(&table[AddressOfSetupEncoder1], 1, 1));
                helperEncoderInit(ENC1_TIMER, mode, resolution);
                break;

            case AddressOfSetupEncoder2:
                sizeInBytes = registerSize[AddressOfSetupEncoder2];
                mode = static_cast<EncoderMode>(bitRead(&table[AddressOfSetupEncoder2], 0, 1));
                resolution = static_cast<EncoderResolution>(bitRead(&table[AddressOfSetupEncoder2], 1, 1));
                helperEncoderStop(ENC2_TIMER, mode, resolution);

                // Copy new configuration
                table[AddressOfSetupEncoder2] = values[addr - first_address];
                length = length - sizeInBytes;
                addr = addr + sizeInBytes;

                // Apply new configuration
                MX_TIM2_Init();
                mode = static_cast<EncoderMode>(bitRead(&table[AddressOfSetupEncoder2], 0, 1));
                resolution = static_cast<EncoderResolution>(bitRead(&table[AddressOfSetupEncoder2], 1, 1));
                helperEncoderInit(ENC2_TIMER, mode, resolution);
                break;

            case AddressOfSetupEncoder3:
                sizeInBytes = registerSize[AddressOfSetupEncoder3];
                mode = static_cast<EncoderMode>(bitRead(&table[AddressOfSetupEncoder3], 0, 1));
                resolution = static_cast<EncoderResolution>(bitRead(&table[AddressOfSetupEncoder3], 1, 1));
                helperEncoderStop(ENC3_TIMER, mode, resolution);

                // Copy new configuration
                table[AddressOfSetupEncoder3] = values[addr - first_address];
                length = length - sizeInBytes;
                addr = addr + sizeInBytes;

                // Apply new configuration
                MX_TIM3_Init();
                mode = static_cast<EncoderMode>(bitRead(&table[AddressOfSetupEncoder3], 0, 1));
                resolution = static_cast<EncoderResolution>(bitRead(&table[AddressOfSetupEncoder3], 1, 1));
                helperEncoderInit(ENC3_TIMER, mode, resolution);
                break;

            case AddressOfSetupPwmPrescaler:
                sizeInBytes = registerSize[AddressOfSetupPwmPrescaler];
                memcpy(&u16, &values[addr - first_address], sizeInBytes);
                memcpy(&table[AddressOfSetupPwmPrescaler], &values[addr - first_address], sizeInBytes);
                PWM_TIMER->Init.Prescaler = u16;
                PWM_TIMER->Instance->PSC = u16;
                length = length - sizeInBytes;
                addr = addr + sizeInBytes;
                break;

            case AddressOfSetupPwmMaxCount:
                sizeInBytes = registerSize[AddressOfSetupPwmMaxCount];
                memcpy(&u16, &values[addr - first_address], sizeInBytes);
                memcpy(&table[AddressOfSetupPwmMaxCount], &values[addr - first_address], sizeInBytes);
                PWM_TIMER->Init.Period = u16;
                PWM_TIMER->Instance->ARR = u16;
                length = length - sizeInBytes;
                addr = addr + sizeInBytes;
                break;

            case AddressOfSetupPwmConfig:
                sizeInBytes = registerSize[AddressOfSetupPwmMaxCount];
                length = length - sizeInBytes;
                addr = addr + sizeInBytes;
                break;

            case AddressOfSetupRadioConfig:
                sizeInBytes = 1;
                table[AddressOfSetupRadioConfig] = values[addr - first_address];

                if (radioIsActive)
                {
                    radio.stopListening();
                    radio.setDataRate(static_cast<rf24_datarate_e>(bitRead(&table[AddressOfSetupRadioConfig], 2, 0b11)));
                    radio.setPALevel(static_cast<rf24_pa_dbm_e>(bitRead(&table[AddressOfSetupRadioConfig], 4, 0b11)));
                    if (bitRead(&table[AddressOfSetupRadioConfig], 0, 0b1))
                        radio.enableDynamicPayloads();
                    else
                        radio.setPayloadSize(32);
                    radio.startListening();
                }

                helperEepromLoad();

                length = length - sizeInBytes;
                addr = addr + sizeInBytes;
                break;

            case AddressOfSetupRadioChannel:
                sizeInBytes = 1;
                table[AddressOfSetupRadioChannel] = values[addr - first_address];
                helperEepromLoad();

                if (radioIsActive)
                {
                    radio.stopListening();
                    radio.setChannel(table[AddressOfSetupRadioChannel]);
                    radio.startListening();
                }

                length = length - sizeInBytes;
                addr = addr + sizeInBytes;
                break;

            case AddressOfSetupRadioBoardAddress:
                sizeInBytes = 5;
                memcpy(&table[AddressOfSetupRadioBoardAddress], &values[addr - first_address], sizeInBytes);
                helperEepromLoad();

                if (radioIsActive)
                {
                    radio.stopListening();
                    radio.closeReadingPipe(1);
                    radio.openReadingPipe(1, &table[AddressOfSetupRadioBoardAddress]);
                    radio.startListening();
                }

                length = length - sizeInBytes;
                addr = addr + sizeInBytes;
                break;

            case AddressOfSetupRadioPcAddress:
                sizeInBytes = 5;
                memcpy(&table[AddressOfSetupRadioPcAddress], &values[addr - first_address], sizeInBytes);
                helperEepromLoad();

                if (radioIsActive)
                {
                    radio.stopListening();
                    radio.openWritingPipe(&table[AddressOfSetupRadioPcAddress]);
                    radio.startListening();
                }

                length = length - sizeInBytes;
                addr = addr + sizeInBytes;
                break;

            case AddressOfSetupEthernetMac:
                sizeInBytes = registerSize[AddressOfSetupEthernetMac];
                memcpy(&table[AddressOfSetupEthernetMac], &values[addr - first_address], sizeInBytes);
                length = length - sizeInBytes;
                addr = addr + sizeInBytes;
                break;

            case AddressOfSetupEthernetIp:
                sizeInBytes = registerSize[AddressOfSetupEthernetIp];
                memcpy(&table[AddressOfSetupEthernetIp], &values[addr - first_address], sizeInBytes);
                length = length - sizeInBytes;
                addr = addr + sizeInBytes;
                break;

            case AddressOfSetupEthernetNetMask:
                sizeInBytes = registerSize[AddressOfSetupEthernetNetMask];
                memcpy(&table[AddressOfSetupEthernetNetMask], &values[addr - first_address], sizeInBytes);
                length = length - sizeInBytes;
                addr = addr + sizeInBytes;
                break;

            case AddressOfSetupEthernetGw:
                sizeInBytes = registerSize[AddressOfSetupEthernetGw];
                memcpy(&table[AddressOfSetupEthernetGw], &values[addr - first_address], sizeInBytes);
                helperEepromLoad();
                helperRestartEthernet();
                length = length - sizeInBytes;
                addr = addr + sizeInBytes;
                break;

            default:
                // If arrived here, then the loop will repeat and it is required
                // to update the addr and length
                length = length - 1;
                addr = addr + 1;
                break;
        }
    }
}

static void protocolMakeResponseForMultiWrite()
{
    commMsgCreateEmptyMsg(&statusPacket, commMsgTypeAnsMultiWrite);
    commMsgUpdateChecksumValue(&statusPacket);
}

static void protocolHandleMultWriteInstruction()
{
    const uint8_t total_length = commMsgGetField(&instructionPacket, commMsgFieldLength) - 1;
    uint8_t* parameters = commMsgGetAddrOfParameters(&instructionPacket);
    uint8_t i = 0;
    uint8_t size;
    uint16_t addr;
    uint8_t* values;

    do
    {
        addr = parameters[i];
        size = parameters[i + 1];
        values = &parameters[i + 2];
        helperProtocolProcessWriteInstruction(addr, size, values);
        i = i + size + 2;

    } while (i < total_length);

    protocolMakeResponseForMultiWrite();
}

static void protocolHandleReadInstruction()
{
    uint8_t firstReg = commMsgGetParameter(&instructionPacket, 0);
    uint8_t length = commMsgGetParameter(&instructionPacket, 1);

    // update memory table
    helperMemoryUpdate();

    commMsgCreateEmptyMsg(&statusPacket, commMsgTypeAnsRead);
    commMsgAppendParameter(&statusPacket, 0);          // insert error
    commMsgAppendParameter(&statusPacket, firstReg);  // insert the first register read
    commMsgAppendParameter(&statusPacket, length);     // insert how many registers were read
    commMsgAppendParameters(&statusPacket, &table[firstReg], length);
    commMsgUpdateChecksumValue(&statusPacket);
}

static void protocolMakeResponseForWrite()
{
    commMsgCreateEmptyMsg(&statusPacket, commMsgTypeAnsWrite);
    commMsgUpdateChecksumValue(&statusPacket);
}

static void protocolHandleWriteInstruction()
{
    uint8_t* parameters = commMsgGetAddrOfParameters(&instructionPacket);
    uint16_t addr = commMsgGetParameter(&instructionPacket, 0);
    int length = commMsgGetParameter(&instructionPacket, 1);
    helperProtocolProcessWriteInstruction(addr, length, &parameters[2]);
    protocolMakeResponseForWrite();
}

void protocolMakeResponseForPing()
{
    commMsgCreateEmptyMsg(&statusPacket, commMsgTypeAnsPing);
    commMsgUpdateChecksumValue(&statusPacket);
}

void protocolMakeAutoReadMsg()
{
    const uint8_t firstReg = table[AddressOfSetupAutoReadFirstReg];
    const uint8_t length = table[AddressOfSetupAutoReadLength];

    // just in case, forces a gpio read
    helperMemoryUpdate();

    commMsgCreateEmptyMsg(&autoReadPacket, commMsgTypeAnsAutoRead);
    commMsgAppendParameter(&autoReadPacket, 0);          // insert error
    commMsgAppendParameter(&autoReadPacket, firstReg);   // insert the first register read
    commMsgAppendParameter(&autoReadPacket, length);     // insert how many registers were read
    commMsgAppendParameters(&autoReadPacket, &table[firstReg], length);
    commMsgUpdateChecksumValue(&autoReadPacket);
}

bool processPacket()
{
    bool generatedStatusPacket = false;

    switch (commMsgGetField(&instructionPacket, commMsgFieldType)) {
        case AS_U8(commMsgTypePing):
            protocolMakeResponseForPing();
            generatedStatusPacket = true;
            break;

        case AS_U8(commMsgTypeRead):
            protocolHandleReadInstruction();
            generatedStatusPacket = true;
            break;

        case AS_U8(commMsgTypeWrite):
            protocolHandleWriteInstruction();
            generatedStatusPacket = true;
            break;

        case AS_U8(commMsgTypeMultiWrite):
            protocolHandleMultWriteInstruction();
            generatedStatusPacket = true;
            break;

        case AS_U8(commMsgTypeReboot):
            HAL_NVIC_SystemReset();
            break;

        default:
            break;
    }

    return generatedStatusPacket;
}

static void helperCommunicationInit()
{
    commReceiverInit(&receiver, &instructionPacket);
    netState = EthernetStatusUninitialized;
    packetFrom = sourceNone;
    autoReadTo = sourceNone;

    // Initialize data reception through UART (bluetooth, zigbee, serial port)
    HAL_UARTEx_ReceiveToIdle_DMA(&huart2, bufferRx, 256);
    __HAL_DMA_DISABLE_IT(huart2.hdmarx, DMA_IT_HT);

    // Now try to find the radio nRF24L01
    _SPI.begin(&hspi1);
    radio.begin(RF24_PB1, RF24_PA10);
    if (radio.isChipConnected())
    {
        // IRQ pin must not use internal pull-up
        // See https://electronics.stackexchange.com/questions/149110/is-the-nrf24l01-irq-line-open-collector
        GPIO_InitTypeDef gpioConf = {
            GPIO_IN_IRQ_Pin,
            GPIO_MODE_IT_FALLING,
            GPIO_NOPULL,
            GPIO_SPEED_FREQ_HIGH
        };
        HAL_GPIO_Init(GPIO_IN_IRQ_GPIO_Port, &gpioConf);
        HAL_NVIC_SetPriority(GPIO_IN_IRQ_EXTI_IRQn, 13, 0);
        HAL_NVIC_EnableIRQ(GPIO_IN_IRQ_EXTI_IRQn);

        // Initialize the radio
        if (radioIsActive)
        {
            radio.maskIRQ(true, true, false);
            radio.setChannel(table[AddressOfSetupRadioChannel]);
            radio.setDataRate(RF24_2MBPS);
            radio.openWritingPipe(&table[AddressOfSetupRadioPcAddress]);
            radio.openReadingPipe(1, &table[AddressOfSetupRadioBoardAddress]);
            radio.setRetries(3, 5);
            radio.startListening();
        }

        radioIsActive = true;
    }
    else
    {
        radioIsActive = false;
    }
}

static void helperOsInit()
{
    osInit();
    hTaskCommunication = osCreateTask(taskCommunication, 4);
    hTaskAutoread      = osCreateTask(taskAutoread,      3);
    hTaskIdle          = osCreateTask(taskIdle,          0);

    osResume(hTaskIdle);
}

void daqInit()
{
    EncoderMode mode;
    EncoderResolution resolution;

    // Initialize the hardware
    helperEepromInit();
    helperI2cInit();
    helperGpioInit();

    mode = static_cast<EncoderMode>(bitRead(&table[AddressOfSetupEncoder1], 0, 1));
    resolution = static_cast<EncoderResolution>(bitRead(&table[AddressOfSetupEncoder1], 1, 1));
    helperEncoderInit(ENC1_TIMER, mode, resolution);

    mode = static_cast<EncoderMode>(bitRead(&table[AddressOfSetupEncoder2], 0, 1));
    resolution = static_cast<EncoderResolution>(bitRead(&table[AddressOfSetupEncoder2], 1, 1));
    helperEncoderInit(ENC2_TIMER, mode, resolution);

    mode = static_cast<EncoderMode>(bitRead(&table[AddressOfSetupEncoder3], 0, 1));
    resolution = static_cast<EncoderResolution>(bitRead(&table[AddressOfSetupEncoder3], 1, 1));
    helperEncoderInit(ENC3_TIMER, mode, resolution);

    helperPwmInit();
    helperOsInit();
    helperCommunicationInit();
}
