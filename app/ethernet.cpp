#include "app/ethernet.h"
#include "app/global_objects.h"
#include <memory.h>

bool isIpValid(uint8_t *ip)
{
    union number_t { uint32_t u32; uint8_t u8[4]; };
    number_t givenIp = { 1 };  // initialize with dumb value just for compiler do not complain
    memcpy(givenIp.u8, ip, 4);      // now copy ip information into the givenIp union

    const bool isZeroIp = givenIp.u32 == 0;           // givenIp == 0.0.0.0 ?
    const bool isLocalIp = givenIp.u32 == 0x7F000001; // givenIp == 127.0.0.1 ?
    return !isZeroIp && !isLocalIp;
}

static inline void wizchipDeselect()
{
    HAL_GPIO_WritePin(GPIO_OUT_CS_GPIO_Port, GPIO_OUT_CS_Pin, GPIO_PIN_SET);
}

static inline void wizchipSelect()
{
    hspi1.Instance->CR1 &= (~SPI_CR1_LSBFIRST_Msk);                                             // set msb first
    hspi1.Instance->CR1 = (hspi1.Instance->CR1 & (~SPI_CR1_BR_Msk)) | SPI_BAUDRATEPRESCALER_4;  // set baud rate
    hspi1.Instance->CR1 &= (~SPI_CR1_CPOL_Msk | SPI_CR1_CPHA_Msk);                              // set mode 0
    HAL_GPIO_WritePin(GPIO_OUT_CS_GPIO_Port, GPIO_OUT_CS_Pin, GPIO_PIN_RESET);
}

static inline void wizchipDisableInterrupts()
{
}

static inline void wizchipEnableInterrupts()
{
}

static inline uint8_t wizchipSpiReadByte()
{
    uint8_t rxData;
    HAL_SPI_Receive(&hspi1, &rxData, 1, 2);
    return rxData;
}

static inline void wizchipSpiWriteByte(uint8_t wb)
{
    HAL_SPI_Transmit(&hspi1, &wb, 1, 2);
}

static inline void wizchipSpiReadBytes(uint8_t* pBuf, uint16_t len)
{
    HAL_SPI_Receive(&hspi1, pBuf, len, 2);
}

static inline void wizchipSpiWriteBytes(uint8_t* pBuf, uint16_t len)
{
    HAL_SPI_Transmit(&hspi1, pBuf, len, 2);
}

static error_t wizchipCheck()
{
    error_t error = errorOk;

#if (_WIZCHIP_ == W5100S)
    /* Read version register */
  if (getVER() != 0x51)
  {
    printf(" ACCESS ERR : VERSION != 0x51, read value = 0x%02x\n", getVER());

    while (1)
      ;
  }
#elif (_WIZCHIP_ == W5300)
    // for further use
#elif (_WIZCHIP_ == W5500)
    /* Read version register */
    if (getVERSIONR() != 0x04)
    {
        error = errorChipNotFound;
    }

    return error;
#endif
}

error_t wizchipInit()
{
    error_t error;
    wizchipDeselect();
    reg_wizchip_cris_cbfunc(wizchipDisableInterrupts, wizchipEnableInterrupts);
    reg_wizchip_cs_cbfunc(wizchipSelect, wizchipDeselect);
    reg_wizchip_spi_cbfunc(wizchipSpiReadByte, wizchipSpiWriteByte);
    reg_wizchip_spiburst_cbfunc(wizchipSpiReadBytes, wizchipSpiWriteBytes);

#if (_WIZCHIP_ == W5100S)
    uint8_t memsize[2][4] = {{2, 2, 2, 2}, {2, 2, 2, 2}};
#elif (_WIZCHIP_ == W5300)
    uint8_t memsize[2][8] = {{8, 8, 8, 8, 8, 8, 8, 8}, {8, 8, 8, 8, 8, 8, 8, 8}};
#elif (_WIZCHIP_ == W5500)
    uint8_t memsize[2][8] = {{2, 2, 2, 2, 2, 2, 2, 2}, {2, 2, 2, 2, 2, 2, 2, 2}};
#endif

    error = wizchipCheck();
    if (error == errorOk)
    {
        // try to initialize the chip
        if (ctlwizchip(CW_INIT_WIZCHIP, (void*) memsize) == -1)
        {
            error = errorInitializationFailed;
        }
    }

    return error;
}

static void netHandleSocket()
{
    switch(getSn_SR(tcpSocket)) {
        case SOCK_ESTABLISHED:
            break;

        case SOCK_CLOSED:
            socket(tcpSocket, tcpProtocol, tcpPort, socketFlag);
            break;

        case SOCK_INIT:
            listen(tcpSocket);
            break;

        case SOCK_CLOSE_WAIT:
            disconnect(tcpSocket);
            break;

        default:
            break;
    }
}

void netPeriodic100ms()
{
    const uint8_t dns[4] = {8, 8, 8, 8};
    error_t error;
    uint8_t phyStatus;

    switch (netState) {
        case EthernetStatusUninitialized:
            error = wizchipInit();
            if (error == errorOk)
            {
                netState = EthernetWaitForCable;
            }
            break;

        case EthernetWaitForCable:
            // check PHY link status
            if (ctlwizchip(CW_GET_PHYLINK, &phyStatus) == 0)
            {
                if (phyStatus == PHY_LINK_ON)
                {
                    // load configuration
                    memcpy(netInfo.mac, &table[AddressOfSetupEthernetMac], 6);
                    memcpy(netInfo.ip, &table[AddressOfSetupEthernetIp], 4);
                    memcpy(netInfo.sn, &table[AddressOfSetupEthernetNetMask], 4);
                    memcpy(netInfo.gw, &table[AddressOfSetupEthernetGw], 4);
                    memcpy(netInfo.dns, dns, 4);
                    netInfo.dhcp = NETINFO_STATIC;

                    // apply configuration
                    ctlnetwork(CN_SET_NETINFO, (void*) &netInfo);

                    // next state
                    netState = EthernetStatusReady;
                }
            }
            else
            {
                netState = EthernetStatusUninitialized;
            }
            break;

        case EthernetStatusReady:
            // check PHY link status
            ctlwizchip(CW_GET_PHYLINK, (void*) &phyStatus);
            if (phyStatus == PHY_LINK_OFF)
            {
                netState = EthernetStatusUninitialized;
            }
            else
            {
                netHandleSocket();
            }
            break;

        default:
            break;
    }
}
